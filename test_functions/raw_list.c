/******************************************************************************
 * Doubly-linked list
 * Copyright (C) 2006-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 * 29th January 2006.
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 13th February 2006: Instrumentation (full of bugs).
 *                   : Don't automatically free a node's data.
 * 21st February 2006: Fix printf format strings.
 * 19th May 2009: Fix a bug in raw_list_node_new().
 * 1st June 2009: Don't free any referenced data in raw_list_delete().
 * 4th June 2009: Should raw_list_node_delete() return the address of the
 *                next or previous existing node? No because if it's used in
 *                the middle of a list, which one should it return?
 * 20th December 2009: Hare and tortoise algorithm for loop detection.
 * 21st December 2009: Refactor.
 * 9th  July     2013: Tidy up for compilation along side test_functions.c.
 ******************************************************************************/

#include <malloc.h>
#include <stdio.h>

#ifndef RAW_LIST
 #include "raw_list.h"
#endif

/************************************/
/* Allocate memory for a list node. */
/************************************/
raw_list_node_t *raw_list_node_allocate(void)
{
	raw_list_node_t *n;
	n=malloc(sizeof(raw_list_node_t));
	return n;
}

/************************************/
/* Free memory used by a list node. */
/************************************/
int raw_list_node_free(raw_list_node_t *n)
{
	free(n);
	return OK;
}

/***************************/
/* Create a new list node. */
/***************************/
raw_list_node_t *raw_list_node_new(void)
{
	raw_list_node_t *n;
	n=raw_list_node_allocate();
	if(n!=(raw_list_node_t *)NULL)
	{
		raw_list_node_init(n);
	}
	return n;
}

/**********************************************************/
/* Delete a list node.                                    */
/* Don't delete any referenced data. Let the user decide. */
/**********************************************************/
int raw_list_node_delete(raw_list_node_t *n)
{
	/* Don't free any data. Let the user decide! */
	/* If there's a previous node, link it to the next */
	if(n->prev!=NULL)
	{
		(n->prev)->next=n->next;
	}
	/* If there's a next node, link it to the previous */
	if(n->next!=NULL)
	{
		(n->next)->prev=n->prev;
	}
	return raw_list_node_free(n);
}

/***************************/
/* Initialise a list node. */
/***************************/
int raw_list_node_init(raw_list_node_t *n)
{
	n->data=(void *)NULL;
	n->prev=(raw_list_node_t *)NULL;
	n->next=(raw_list_node_t *)NULL;
	return OK;
}

/*****************************************************************/
/* Start a new list i.e. create a new node initialised to empty. */
/*****************************************************************/
raw_list_node_t *raw_list_new(void)
{
	return raw_list_node_new();
}

/**********************************************************/
/* Delete an entire list.                                 */
/* Don't delete any referenced data. Let the user decide. */
/**********************************************************/
raw_list_node_t *raw_list_delete(raw_list_node_t *n)
{
	raw_list_node_t *m;
	n=raw_list_last(n);
	do
	{
		m=n->prev;
        /*
         * FIXME: This doesn't free any malloc()'d data.
         */
		raw_list_node_delete(n);
		n=m;
	}
	while(n!=(raw_list_node_t *)NULL);
	return n;
}

/**********************************************************/
/* Return a pointer to the next node in the list or NULL. */
/**********************************************************/
raw_list_node_t *raw_list_next(raw_list_node_t *n)
{
	return n->next;
}

/**************************************************************/
/* Return a pointer to the previous node in the list or NULL. */
/**************************************************************/
raw_list_node_t *raw_list_prev(raw_list_node_t *n)
{
	return n->prev;
}

/***********************************************************/
/* Return a pointer to the first node in the list or NULL. */
/***********************************************************/
raw_list_node_t *raw_list_first(raw_list_node_t *n)
{
	while(n->prev!=(raw_list_node_t *)NULL) n=n->prev;
	return n;
}

/***********************************************************/
/* Return a pointer to the final node in the list or NULL. */
/***********************************************************/
raw_list_node_t *raw_list_last(raw_list_node_t *n)
{
	while(n->next!=(raw_list_node_t *)NULL) n=n->next;
	return n;
}

/****************************************************************************/
/* Assign some data to the data pointer in a node.                          */
/****************************************************************************/
raw_list_node_t *raw_list_assign(raw_list_node_t *n, void *d)
{
	n->data=d;
	return n;
}

/***************************************************************/
/* Unassign the data referenced by the data pointer in a node. */
/* The user should decide whether the data needs freeing.      */
/***************************************************************/
int raw_list_unassign(raw_list_node_t *n)
{
	n->data=NULL;
	return 0;
}

/***********************************************/
/* Insert a new node in the list after node n. */
/***********************************************/
raw_list_node_t *raw_list_node_insert(raw_list_node_t *n)
{
	raw_list_node_t *m;
	if(n!=(raw_list_node_t *)NULL)
	{
		m=raw_list_node_new();
		if(m==(raw_list_node_t *)NULL) return m;
		m->prev=n;
		m->next=n->next;
		(n->next)->prev=m;
		n->next=m;
		return m;
	}
	return n;
}

/*********************************************/
/* Append a new node at the end of the list. */
/*********************************************/
raw_list_node_t *raw_list_node_append(raw_list_node_t *n)
{
	raw_list_node_t *m;

	n=raw_list_last(n);
	if(n==(raw_list_node_t *)NULL) return n;

	m=raw_list_node_new();
	if(m==(raw_list_node_t *)NULL) return m;

	n->next=m;
	m->prev=n;

	return m;
}

/****************************************************/
/* Prepend a new node at the beginning of the list. */
/****************************************************/
raw_list_node_t *raw_list_node_prepend(raw_list_node_t *n)
{
	raw_list_node_t *m;
	n=raw_list_first(n);
	if(n==(raw_list_node_t *)NULL) return n;
	m=raw_list_node_new();
	if(m==(raw_list_node_t *)NULL) return m;
	n->prev=m;
	m->next=n;
	m->prev=(raw_list_node_t *)NULL;
	return m;
}

/****************************************************/
/* Return the data pointer from the specified node. */
/****************************************************/
void *raw_list_get_data(raw_list_node_t *n)
{
	return n->data;
}

/*************************************************************************/
/* Look for loops in the list using the Hare and the Tortoise algorithm. */
/*************************************************************************/
raw_list_node_t *raw_list_forward_loop_check(raw_list_node_t *n)
{
	raw_list_node_t *m;

	m=n->next;

	while((m!=NULL) && (m!=n) && ((n=raw_list_next(n))!=NULL) &&
		((m=raw_list_next(m))!=NULL))
	{
		m=raw_list_next(m);
	}

	if(m==n)
		return m;
	else
		return NULL;
}

raw_list_node_t *raw_list_backward_loop_check(raw_list_node_t *n)
{
	raw_list_node_t *m;

	m=n->prev;

	while((m!=NULL) && (m!=n) && ((n=raw_list_prev(n))!=NULL) &&
		((m=raw_list_prev(m))!=NULL))
	{
		m=raw_list_prev(m);
	}

	if(m==n)
		return m;
	else
		return NULL;
}

