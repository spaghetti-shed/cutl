/******************************************************************************
 * test_functions (test)
 * Mon Nov 15 22:36:16 GMT 2010
 * Copyright (C) 2010-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 17th November 2010: Use new assertions library.
 * 18th November 2010: Frob with assertions.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "../assertions/assertions.h"
#include "test_functions.h"

void test_initialisation();
void test_accessors();
void test_override();

int main(int argc, char *argv[])
{
	printf("test_functions test.\n");

	test_initialisation();
	test_accessors();
	test_override();

	return 0;
}

void test_initialisation()
{
	printf("test_initialisation()\n");

	TEST_OBSERVATION("foo", 0, counting_malloc_get_call_count());
	TEST_OBSERVATION("foo", 0, counting_malloc_get_success_count());
	TEST_OBSERVATION("foo", 0, counting_malloc_get_fail_count());

	TEST_OBSERVATION("foo", 0, counting_free_get_call_count());
	TEST_OBSERVATION("foo", 0, counting_free_get_success_count());
	TEST_OBSERVATION("foo", 0, counting_free_get_fail_count());
}

void test_accessors()
{
	void *ptr;
	printf("test_accessors()\n");

	ptr=counting_malloc(1);
	TEST_OBSERVATION("counting_malloc", 1, (NULL!=ptr));
	TEST_OBSERVATION("foo", 1, counting_malloc_get_call_count());
	TEST_OBSERVATION("foo", 1, counting_malloc_get_success_count());
	TEST_OBSERVATION("foo", 0, counting_malloc_get_fail_count());

	counting_free(ptr);
	TEST_OBSERVATION("foo", 1, counting_free_get_call_count());
	TEST_OBSERVATION("foo", 1, counting_free_get_success_count());
	TEST_OBSERVATION("foo", 0, counting_free_get_fail_count());
}

void test_override()
{
	void *ptr;
	printf("test_override()\n");

	counting_malloc_reset();
	counting_free_reset();

	counting_malloc_set_override(NULL);
	ptr=counting_malloc(1);
	TEST_OBSERVATION("counting_malloc", 1, (NULL==ptr));
	TEST_OBSERVATION("foo", 1, counting_malloc_get_call_count());
	TEST_OBSERVATION("foo", 0, counting_malloc_get_success_count());
	TEST_OBSERVATION("foo", 1, counting_malloc_get_fail_count());

	counting_free(ptr);
	TEST_OBSERVATION("foo", 1, counting_free_get_call_count());
	TEST_OBSERVATION("foo", 0, counting_free_get_success_count());
	TEST_OBSERVATION("foo", 1, counting_free_get_fail_count());

	counting_malloc_reset_override();
	ptr=counting_malloc(1);
	TEST_OBSERVATION("counting_malloc", 0, (NULL==ptr));
	TEST_OBSERVATION("foo", 2, counting_malloc_get_call_count());
	TEST_OBSERVATION("foo", 1, counting_malloc_get_success_count());
	TEST_OBSERVATION("foo", 1, counting_malloc_get_fail_count());

	counting_free_set_override();
	counting_free(ptr);
	TEST_OBSERVATION("foo", 2, counting_free_get_call_count());
	TEST_OBSERVATION("foo", 0, counting_free_get_success_count());
	TEST_OBSERVATION("foo", 2, counting_free_get_fail_count());

	counting_free_reset_override();
	counting_free(ptr);
	TEST_OBSERVATION("foo", 3, counting_free_get_call_count());
	TEST_OBSERVATION("foo", 1, counting_free_get_success_count());
	TEST_OBSERVATION("foo", 2, counting_free_get_fail_count());
}
