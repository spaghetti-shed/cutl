/******************************************************************************
 * test_functions
 * Mon Nov 15 22:36:16 GMT 2010
 * Copyright (C) 2010-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 18th November 2010: Overrides for implementing stub behaviour.
 * 27th November 2010: malloc() params list to remember malloc()s for
 *                     instrumentation.
 * 2011-04-22        : Turn off the verbose output.
 * 2018-08-09        : Fix broken #include.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "raw_list.h"
#include "test_functions.h"

typedef struct
{
	size_t size;
	void *address;
}
malloc_params_t;

static function_stats_t counting_malloc_stats;
static function_stats_t counting_free_stats;

static int32_t counting_malloc_override;
static void *counting_malloc_override_return;
static int32_t counting_free_override;

raw_list_node_t *remember;

void *counting_malloc(size_t size)
{
	void *ptr;
	raw_list_node_t *r;
	malloc_params_t *m;

	if(1==counting_malloc_override)
	{
		ptr=counting_malloc_override_return;
	}
	else
	{
		ptr=malloc(size);
		if(NULL==remember)
		{
			remember=raw_list_new();
			assert(NULL!=remember);
			r=remember;
		}
		else
		{
			r=raw_list_node_append(remember);
			assert(NULL!=r);
		}
		m=malloc(sizeof(malloc_params_t));
		assert(NULL!=m);
		m->size=size;
		m->address=ptr;
		raw_list_assign(r, (void *)m);

#ifdef TEST_FUNCTIONS_DEBUG
		counting_malloc_dump_remember();
#endif
	}
	counting_malloc_stats.call_count++;
	if(NULL!=ptr)
	{
		counting_malloc_stats.success_count++;
	}
	else
	{
		counting_malloc_stats.fail_count++;
	}

	return ptr;
}

void counting_malloc_reset(void)
{
	counting_malloc_stats.call_count=0;
	counting_malloc_stats.success_count=0;
	counting_malloc_stats.fail_count=0;
	counting_malloc_override=0;
}

int32_t counting_malloc_get_call_count(void)
{
	return counting_malloc_stats.call_count;
}

int32_t counting_malloc_get_success_count(void)
{
	return counting_malloc_stats.success_count;
}

int32_t counting_malloc_get_fail_count(void)
{
	return counting_malloc_stats.fail_count;
}

void counting_malloc_set_override(void *ptr)
{
	counting_malloc_override=1;
	counting_malloc_override_return=ptr;
}

void counting_malloc_reset_override(void)
{
	counting_malloc_override=0;
}


void counting_malloc_dump_remember(void)
{
	raw_list_node_t *current;
	malloc_params_t *p;

	printf("counting_malloc_dump_remember()\n");
	current=remember;

	while(NULL!=current)
	{
		p=raw_list_get_data(current);
		printf("Address: %p, size: %d\n", p->address, (int)p->size);
		current=raw_list_next(current);
	}
}

void counting_free(void *ptr)
{
	raw_list_node_t *current;
	raw_list_node_t *next;
	malloc_params_t *p;

	counting_free_stats.call_count++;

	if(1==counting_free_override)
	{
		counting_free_stats.fail_count++;
	}
	else if(NULL!=ptr)
	{
		current=raw_list_first(remember);
		while(NULL!=current)
		{
			next=raw_list_next(current);
			p=raw_list_get_data(current);
			/*printf("counting_free(): %p\n", p);*/
			if(p->address==ptr)
			{
				/*printf("counting_free(): found %p\n", ptr);*/
				free(ptr);
				counting_free_stats.success_count++;
				free(p);
				raw_list_node_delete(current);
				if(current==remember)
				{
					remember=next;
				}
#ifdef TEST_FUNCTIONS_DEBUG
				counting_malloc_dump_remember();
#endif
				break;
			}
			current=next;
		}

		if(NULL==current)
		{
			/* We got all the way through the list without */
			/* finding the pointer. It must be invalid.    */
			/*printf("counting_free(): NOT found %p\n", ptr);*/
			counting_free_stats.fail_count++;
		}
	}
	else
	{
		counting_free_stats.fail_count++;
	}
}

void counting_free_reset(void)
{
	counting_free_stats.call_count=0;
	counting_free_stats.success_count=0;
	counting_free_stats.fail_count=0;
	counting_free_override=0;
}

int32_t counting_free_get_call_count(void)
{
	return counting_free_stats.call_count;
}

int32_t counting_free_get_success_count(void)
{
	return counting_free_stats.success_count;
}

int32_t counting_free_get_fail_count(void)
{
	return counting_free_stats.fail_count;
}


void counting_free_set_override(void)
{
	counting_free_override=1;
}

void counting_free_reset_override(void)
{
	counting_free_override=0;
}

