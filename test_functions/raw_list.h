/******************************************************************************
 * Doubly-linked list (header)
 * Copyright (C) 2006-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 * 29th January 2006.
 *
 * Header file containing macros, typedefs and function prototypes.
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 19th May 2009: To-do: Split into public and private functions.
 * 21st May 2009: D'oh!
 * 20th December 2009: Hare and tortoise algorithm for loop detection.
 * 2013-12-11: C++ externs.
 * 2014-05-28: Tidy up.
 * 2016-09-20: Warning: Do not use this. It is rubbish.
 ******************************************************************************/

#ifndef __RAW_LIST_H__
#define __RAW_LIST_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define OK      0

struct raw_list_node_t
{
	/* pointer to the object holding the data */
    void *data;
	/* links */
    struct raw_list_node_t *prev, *next;
};

typedef struct raw_list_node_t raw_list_node_t;

/***********************/
/* function prototypes */
/***********************/

raw_list_node_t *raw_list_node_allocate(void);
int raw_list_node_free(raw_list_node_t *);

raw_list_node_t *raw_list_node_new(void);
int raw_list_node_delete(raw_list_node_t *);
int raw_list_node_init(raw_list_node_t *);

raw_list_node_t *raw_list_new(void);
raw_list_node_t *raw_list_delete(raw_list_node_t *);

raw_list_node_t *raw_list_next(raw_list_node_t *);
raw_list_node_t *raw_list_prev(raw_list_node_t *);

raw_list_node_t *raw_list_first(raw_list_node_t *);
raw_list_node_t *raw_list_last(raw_list_node_t *);

raw_list_node_t *raw_list_assign(raw_list_node_t *, void *);
int raw_list_unassign(raw_list_node_t *);

raw_list_node_t *raw_list_node_insert(raw_list_node_t *);
raw_list_node_t *raw_list_node_append(raw_list_node_t *);
raw_list_node_t *raw_list_node_prepend(raw_list_node_t *);

void *raw_list_get_data(raw_list_node_t *);

raw_list_node_t *raw_list_forward_loop_check(raw_list_node_t *);
raw_list_node_t *raw_list_backward_loop_check(raw_list_node_t *);

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* __RAW_LIST_H__ */

