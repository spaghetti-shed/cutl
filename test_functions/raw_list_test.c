/******************************************************************************
 * raw_list_test
 * Copyright (C) 2006-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 13th February 2006 Looking for bugs.
 * 17th May 2009: Fix compile warnings.
 * 19th May 2009: Tidy up.
 * 21st May 2009: More tidy ups and tests.
 * 31st May 2009: More tidy ups and tests. More nodes.
 * 1st June 2009: Node insertion and deletion.
 * 4th June 2009: Node insertion and deletion.
 * 15th December 2009: Append and prepend tests.
 * 20th December 2009: Actually run the append and prepend tests.
 *                     Ideas: Shuffling nodes (sorting?).
 *                            Hare and tortoise algorithm.
 * 10th July     2013: Fix compiler warnings.
 * 28th May      2014: Tidy up.
 ******************************************************************************/

#include <malloc.h>
#include <stdio.h>
#include <assert.h>

#include "raw_list.h"

#define NUMBER	10
#define NNODES	5

int old_test(void);
int node_test(void);
int multiple_node_test(void);
int list_delete_test(void);
int insert_delete_test(void);
int append_prepend_test(void);
int forward_loop_detection_test(void);
int backward_loop_detection_test(void);

int count_assigned_nodes(raw_list_node_t *l);

int main(void)
{
	printf("raw_list test.\n");

	old_test();
	node_test();
	multiple_node_test();
	insert_delete_test();
	append_prepend_test();
	forward_loop_detection_test();
	backward_loop_detection_test();

	printf("Done.\n");
	return 0;
}
 
/******************************************************************************/
/* This is the old test code which relies too much on a human inspecting the  */
/* output. It will be kept until the new unit tests are comprehensive enough. */
/******************************************************************************/
int old_test(void)
{
	raw_list_node_t *n, *m;
	int data[NUMBER];
	int i;

	printf("raw_list old test.\n");

	n = raw_list_new();
	printf("new raw_list: %p\n", (void *)n);

	printf("assigning...\n");
	for(i = 0; i < NUMBER; i++)
	{
		data[i] = i;
		m = raw_list_node_append(n);
		raw_list_assign(n, (void *)&data[i]);
		printf("i: %d, *n->data: %d\n", i, *(int *)n->data);
		n = m;
	}

	n = raw_list_first(n);
	printf("first node: %p\n", (void *)n);

	printf("reading...\n");
	for(i = 0; i < NUMBER; i++)
	{
		printf("i: %d, *n->data: %d\n", i, *(int *)n->data);
		n = raw_list_next(n);
	}

	printf("deleting...\n");
	raw_list_delete(n);

	printf("Done.\n");

	return 0;
}

/******************************************************************************/
/* node_test: Create a list node and test assigning and reading data.         */
/******************************************************************************/
int node_test(void)
{
	raw_list_node_t *node;
	raw_list_node_t *node2;
	void *data;
	char *message = "This is a test!";

	printf("node_test\n");

    /* Make a node. */
	printf("\nraw_list_node_new()\n");
	node = raw_list_node_new();
	assert(NULL != node);

    /* Get the address of the data pointer to in the node. */
	printf("\nraw_list_get_data(node)\n");
	data = raw_list_get_data(node);
	assert(NULL == data);

	/* Assign some data and test whether it worked. */
	printf("\nraw_list_assign()\n");
	node2 = raw_list_assign(node, (void *)message);
	assert(node == node2);
	assert((void *)message == (void *)raw_list_get_data(node));

	/* Try to go to the next node. */
	printf("\nraw_list_next()\n");
	node2 = raw_list_next(node);
	assert(NULL == node2);

	/* Try to go to the previous node. */
	printf("\nraw_list_prev()\n");
	node2 = raw_list_prev(node);
	assert(NULL == node2);

	/* Try to go to the first node. */
	printf("\nraw_list_first()\n");
	node2 = raw_list_first(node);
	assert(node == node2);

	/* Try to go to the last node. */
	printf("\nraw_list_last()\n");
	node2 = raw_list_last(node);
	assert(node == node2);

	/* Un-assign some data and test whether it worked. */
	printf("\nraw_list_unassign()\n");
	assert(raw_list_unassign(node) == 0);
	assert((void *)NULL == (void *)raw_list_get_data(node));

	/* Can we delete individual nodes? */
	printf("\nraw_list_node_delete()\n");
	assert(0 == raw_list_node_delete(node));

	printf("node_test done\n");

	return 0;
}

/******************************************************************************/
/* multiple_node_test: Test a multi-node list, assigning and reading data.    */
/******************************************************************************/
int multiple_node_test(void)
{
	raw_list_node_t *nodes[NNODES];
	raw_list_node_t *node_a;
	raw_list_node_t *node_b;
	char *strings[] = {"one", "two", "three", "four", "five"};
	int i;
	int result;

	printf("multiple_node_test\n");

    /* Make a list. */
	printf("\nraw_list_new()\n");

	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);
	for(i = 1; i < NNODES; i++)
	{
		nodes[i] = raw_list_node_append(nodes[i - 1]);
		assert(NULL != nodes[i]);
	}

	/* Go through the list checking the next pointers. */
	printf("\nraw_list_next()\n");
	i = 0;
	node_a = nodes[i++];
	while((node_b = raw_list_next(node_a)) != NULL)
	{
		assert(node_b == nodes[i++]);
		node_a = node_b;
	}
	assert(NNODES == i);
	

	/* Go through the list checking the prev pointers. */
	printf("\nraw_list_prev()\n");
	i = NNODES-1;
	node_a=nodes[i--];
	while((node_b = raw_list_prev(node_a)) != NULL)
	{
		assert(node_b == nodes[i--]);
		node_a = node_b;
	}
	assert(-1 == i);

	/* Test finding the head. */
	printf("\nraw_list_first()\n");
	for(i = NNODES - 1; i >= 0; i--)
	{
		node_a = raw_list_first(nodes[i]);
		assert(nodes[0] == node_a);
	}

	/* Test finding the tail. */
	printf("\nraw_list_last()\n");
	for(i = 0; i < NNODES; i++)
	{
		node_a = raw_list_last(nodes[i]);
		assert(nodes[NNODES-1] == node_a);
	}

	/* Test assigning data to the first node. */
	printf("\nraw_list_assign()\n");
	node_a = raw_list_assign(nodes[0], strings[0]);
	assert(node_a == nodes[0]);
	printf("data: %s\n", (char *)raw_list_get_data(nodes[0]));

	/* Count number of nodes with assigned data. */
	assert(1 == count_assigned_nodes(nodes[0]));

	/* Assign to the last node. */
	printf("\nraw_list_assign()\n");
	node_a = raw_list_last(nodes[0]);
	assert(node_a == nodes[NNODES - 1]);
	node_a = raw_list_assign(node_a, strings[0]);
	assert(raw_list_get_data(node_a) == raw_list_get_data(nodes[0]));

	/* Count number of nodes with assigned data. */
	assert(2 == count_assigned_nodes(nodes[0]));

	/* Unassign the data in the first node. */
	printf("\nraw_list_unassign()\n");
	result = raw_list_unassign(nodes[0]);
	assert(0 == result);
	assert(NULL == raw_list_get_data(nodes[0]));

	/* Count number of nodes with assigned data. */
	assert(1 == count_assigned_nodes(nodes[0]));

	/* Unassign the data in the last node. */
	printf("\nraw_list_unassign()\n");
	result = raw_list_unassign(nodes[NNODES - 1]);
	assert(0 == result);
	assert(NULL == raw_list_get_data(nodes[NNODES - 1]));

	/* Count number of nodes with assigned data. */
	assert(0 == count_assigned_nodes(nodes[0]));

	/* Test assign and unassign to the whole list. */
	printf("\nraw_list_assign()\n");
	node_a=nodes[0];
	do
	{
		node_b = raw_list_assign(node_a, strings[0]);
		assert(node_b == node_a);
	}
	while((node_a = raw_list_next(node_a)) != NULL);
	assert(NNODES == count_assigned_nodes(nodes[0]));

	printf("\nraw_list_unassign()\n");
	node_a = nodes[0];
	do
	{
		assert(0 == raw_list_unassign(node_a));
	}
	while((node_a = raw_list_next(node_a)) != NULL);
	assert(0 == count_assigned_nodes(nodes[0]));

	/* Free up allocated nodes. (Not a test) */
	printf("\ndeleting allocated list nodes\n");
	for(i = 0; i < NNODES; i++)
	{
		raw_list_node_delete(nodes[i]);
	}

	printf("multiple_node_test done\n");

	return 0;
}

/******************************************************************************/
/* list_delete_test: Delete a multi-node list with assigned data.             */
/******************************************************************************/
int list_delete_test(void)
{
	raw_list_node_t *nodes[NNODES];
	raw_list_node_t *node_a;
	raw_list_node_t *node_b;
	char *strings[] = {"one", "two", "three", "four", "five"};
	int i;

	printf("list_delete_test\n");

    /* Make a list. */
	printf("\nraw_list_new()\n");

	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);
	node_a = raw_list_assign(nodes[0], strings[0]);
	assert(node_a == nodes[0]);

	for(i = 1; i < NNODES; i++)
	{
		nodes[i] = raw_list_node_append(nodes[i - 1]);
		assert(NULL != nodes[i]);
        node_a = raw_list_assign(nodes[i], strings[i]);
        assert(node_a == nodes[1]);
	}

	/* Count number of nodes with assigned data. */
	assert(5 == count_assigned_nodes(nodes[0]));

    node_b = raw_list_delete(nodes[0]);
    assert(NULL == node_b);

	printf("multiple_node_test done\n");

	return 0;
}

int insert_delete_test(void)
{
	raw_list_node_t *nodes[NNODES];
	int i;

	printf("insert_delete_test.\n");

    /* Make a list. */
	printf("\nraw_list_new()\n");

	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);
	for(i = 1; i < NNODES; i++)
	{
		nodes[i] = raw_list_node_append(nodes[i - 1]);
		assert(NULL != nodes[i]);
	}

	/* Check that we can delete nodes and that the references are updated. */
	printf("\nraw_list_node_delete() forwards\n");
	for(i = 0; i < NNODES - 1; i++)
	{
		assert(NULL == raw_list_prev(nodes[i]));
		assert(raw_list_next(nodes[i]) == nodes[i + 1]);
		assert(raw_list_prev(nodes[i + 1]) == nodes[i]);
		assert(0 == raw_list_node_delete(nodes[i]));
		assert(raw_list_prev(nodes[i + 1]) == NULL);
	}

	/* ..and the final node: */
	assert(raw_list_next(nodes[i]) == NULL);
	assert(0 == raw_list_node_delete(nodes[i]));

    /* Make a list. */
	printf("\nraw_list_new()\n");

	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);
	for(i = 1; i < NNODES; i++)
	{
		nodes[i] = raw_list_node_append(nodes[i - 1]);
		assert(NULL != nodes[i]);
	}

	/* Check that we can delete nodes and that the references are updated. */
	printf("\nraw_list_node_delete() backwards\n");
	for(i = NNODES - 1; i > 0; i--)
	{
		assert(i > 0);
		assert(NULL == raw_list_next(nodes[i]));
		assert(raw_list_prev(nodes[i]) == nodes[i - 1]);
		assert(raw_list_next(nodes[i - 1]) == nodes[i]);
		assert(0 == raw_list_node_delete(nodes[i]));
		assert(raw_list_next(nodes[i - 1]) == NULL);
	}

	/* ..and the final node: */
	assert(0 == i);
	assert(raw_list_prev(nodes[i]) == NULL);
	assert(0 == raw_list_node_delete(nodes[i]));

	/* FIXME: We haven't tested insertion. */

	return 0;
}

int append_prepend_test(void)
{
	raw_list_node_t *nodes[NNODES];
	int i;

	printf("append_prepend_test.\n");

	printf("\nraw_list_new()\n");

	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);

	/* Test prepend. */
	nodes[1] = raw_list_node_prepend(nodes[0]);
	assert(NULL != nodes[1]);
	assert(raw_list_prev(nodes[0]) == nodes[1]);
	assert(raw_list_next(nodes[1]) == nodes[0]);

	/* Prepend again from original node.*/
	nodes[2] = raw_list_node_prepend(nodes[0]);
	assert(NULL!=nodes[2]);
	/* Check that the first hasn't been corrupted. */
	assert(raw_list_prev(nodes[0]) == nodes[1]);
	assert(raw_list_next(nodes[1]) == nodes[0]);
	/* Check that the second new node is in the expected place. */
	assert(raw_list_prev(nodes[1]) == nodes[2]);
	assert(raw_list_next(nodes[2]) == nodes[1]);

	/* Append from the very beginning. */
	nodes[3] = raw_list_node_append(nodes[2]);
	assert(NULL != nodes[3]);
	assert(raw_list_next(nodes[0]) == nodes[3]);
	assert(raw_list_prev(nodes[3]) == nodes[0]);

	/* Append from the most recently-added node. */
	nodes[4] = raw_list_node_append(nodes[3]);
	assert(NULL != nodes[4]);
	assert(raw_list_next(nodes[3]) == nodes[4]);
	assert(raw_list_prev(nodes[4]) == nodes[3]);

	/* Check that we can find the first and last nodes. */
	assert(raw_list_first(nodes[0]) == nodes[2]);
	assert(raw_list_last(nodes[0]) == nodes[4]);

	/* Delete nodes (not a test). */
	/* for(i=0; i<NNODES; i++) */
	for(i = 0; i < 5; i++)
	{
		raw_list_node_delete(nodes[i]);
	}

	return 0;
}

int forward_loop_detection_test(void)
{
	raw_list_node_t *nodes[NNODES];
	int i;

	printf("forward_loop_detection_test.\n");

    /* Make a list. */
	printf("\nraw_list_new()\n");

	printf("1 node\n");
	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);

	/* Check for a loop. */
	assert(raw_list_forward_loop_check(nodes[0])==0);

	for(i=1; i<NNODES; i++)
	{
		printf("%d nodes\n",i+1);
		nodes[i] = raw_list_node_append(nodes[0]);
		assert(raw_list_forward_loop_check(nodes[0]) == 0);
	}

	printf("Making node 4 point to node 0\n");
	nodes[4]->next = nodes[0];
	assert(raw_list_forward_loop_check(nodes[0]) == nodes[4]);

	printf("Making node 3 point to node 2\n");
	nodes[3]->next = nodes[2];
	assert(raw_list_forward_loop_check(nodes[0]) == nodes[3]);

	printf("Making node 1 point to node 0\n");
	nodes[1]->next = nodes[0];
	assert(raw_list_forward_loop_check(nodes[0]) == nodes[1]);

	printf("Making node 0 point to itself\n");
	nodes[0]->next = nodes[0];
	assert(raw_list_forward_loop_check(nodes[0]) == nodes[0]);

	/* Delete nodes (not a test). */
	/* for(i=0; i<NNODES; i++) */
	for(i=0; i<5; i++)
	{
		raw_list_node_delete(nodes[i]);
	}
    return 0;
}

int backward_loop_detection_test(void)
{
	raw_list_node_t *nodes[NNODES];
	int i;

	printf("backward_loop_detection_test.\n");

    /* Make a list. */
	printf("\nraw_list_new()\n");

	printf("1 node\n");
	nodes[0] = raw_list_new();
	assert(NULL != nodes[0]);

	/* Check for a loop. */
	assert(raw_list_forward_loop_check(nodes[0]) == 0);

	for(i = 1; i < NNODES; i++)
	{
		printf("%d nodes\n",i+1);
		nodes[i] = raw_list_node_prepend(nodes[0]);
		assert(raw_list_backward_loop_check(nodes[0]) == 0);
	}

	printf("Making node 4 point to node 0\n");
	assert(NULL == nodes[4]->prev);
	nodes[4]->prev = nodes[0];
	assert(raw_list_backward_loop_check(nodes[0]) == nodes[4]);

	printf("Making node 3 point to node 2\n");
	assert(nodes[4] == nodes[3]->prev);
	nodes[3]->prev = nodes[2];
	assert(raw_list_backward_loop_check(nodes[0]) == nodes[3]);

	printf("Making node 1 point to node 0\n");
	assert(nodes[2] == nodes[1]->prev);
	nodes[1]->prev = nodes[0];
	assert(raw_list_backward_loop_check(nodes[0]) == nodes[1]);

	printf("Making node 0 point to itself\n");
	nodes[0]->prev = nodes[0];
	assert(raw_list_backward_loop_check(nodes[0]) == nodes[0]);

	/* Delete nodes (not a test). */
	/* for(i=0; i<NNODES; i++) */
	for(i = 0; i < 5; i++)
	{
		raw_list_node_delete(nodes[i]);
	}
    return 0;
}

int count_assigned_nodes(raw_list_node_t *l)
{
	raw_list_node_t *current;
	int nassigned = 0;

	current = raw_list_first(l);
	do
	{
		if(NULL != raw_list_get_data(current)) nassigned++;
	}
	while((current = raw_list_next(current)) != NULL);

	return nassigned;
}

