/******************************************************************************
 * test_functions (header)
 * Mon Nov 15 22:36:16 GMT 2010
 * Copyright (C) 2010-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 18th November 2010: Overrides for implementing stub behaviour.
 * 2013-12-11: C++ externs.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __TEST_FUNCTIONS_H__
#define __TEST_FUNCTIONS_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct
{
	int32_t call_count;
	int32_t success_count;
	int32_t fail_count;
}
function_stats_t;


void *counting_malloc(size_t size);
void counting_free(void *ptr);

void counting_malloc_reset(void);
int32_t counting_malloc_get_call_count(void);
int32_t counting_malloc_get_success_count(void);
int32_t counting_malloc_get_fail_count(void);
void counting_malloc_set_override(void *ptr);
void counting_malloc_reset_override(void);

void counting_malloc_dump_remember(void);

void counting_free_reset(void);
int32_t counting_free_get_call_count(void);
int32_t counting_free_get_success_count(void);
int32_t counting_free_get_fail_count(void);
void counting_free_set_override(void);
void counting_free_reset_override(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __TEST_FUNCTIONS_H__ */

