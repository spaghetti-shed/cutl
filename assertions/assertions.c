/******************************************************************************
 * assertions
 * Wed Nov 17 21:33:23 GMT 2010
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 17th November 2010 Taken from module_object.c.
 * 2011-04-22         Integers, pointers and strings. wrapped_exit().
 * 2013-05-31         size_t.
 * 2013-12-11         float.
 * 2014-02-09         Fix some types broken when doing float.
 * 2014-04-12         Add support for pass/fail callbacks and continuing on
 *                    failure of an assertion.
 * 2014-06-25         64-bit integers.
 * 2014-06-29         Unsigned 64-bit integers.
 * 2014-08-17         Fix types in 64-bit integer test functions.
 * 2014-11-27         Add accessors to reset pass and fail counts for
 *                    testing.
 * 2014-12-01         Add floating point range tests.
 * 2015-01-06         Add double-precision floating point tests.
 * 2015-01-12         Add q31 fixed-point tests.
 * 2015-09-13         Add uint32_t tests.
 * 2024-09-26         Add macros for testing floats and doubles to within a
 *                    tolerance specified as a fraction of 1 e.g. 0.01.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "assertions.h"

#ifdef TESTING
#define exit wrapped_exit
#endif

typedef struct
{
    int32_t call_count;
}
exit_stats_t;

static exit_stats_t exit_stats;
static int32_t do_not_stop;
static void (*fail_callback)(void);
static void (*pass_callback)(void);
static int32_t fail_count;
static int32_t pass_count;

/* Accessors for unit testing the tests. */
void wrapped_exit_reset_call_count()
{
    exit_stats.call_count = 0;
}

int32_t wrapped_exit_get_call_count()
{
    return exit_stats.call_count;
}

void wrapped_exit(int status)
{
    exit_stats.call_count++;
}

void fail_test(int32_t code)
{
    if (1 == do_not_stop)
    {
        fail_count++;
        if (NULL != fail_callback)
        {
            fail_callback();
        }
    }
    else
    {
        exit(code);
    }
}

void pass_test(void)
{
    pass_count++;
    if (NULL != pass_callback)
    {
        pass_callback(); 
    }
}

/* FIXME: This is no use for comparing pointers. */
void test_observation(char *file, int32_t line, char *message, int32_t expected,
                      int32_t observed)
{
    if(NULL == file)
    {
        printf("test_observation(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_observation(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %d, observed: %d.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_int32_t_equal(char *file, int32_t line, char *message,
                        int32_t expected, int32_t observed)
{
    if(NULL == file)
    {
        printf("test_int32_t_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_int32_t_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %d, observed: %d.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_int32_t_not_equal(char *file, int32_t line, char *message,
                            int32_t expected, int32_t observed)
{
    if(NULL == file)
    {
        printf("test_int32_t_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_int32_t_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %d, observed: %d.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_int32_t_zero(char *file, int32_t line, char *message,
                       int32_t observed)
{
    test_int32_t_equal(file, line, message, 0, observed);
}

void test_int32_t_not_zero(char *file, int32_t line, char *message,
                           int32_t observed)
{
    test_int32_t_not_equal(file, line, message, 0, observed);
}

void test_uint32_t_equal(char *file, int32_t line, char *message,
                         uint32_t expected, uint32_t observed)
{
    if(NULL == file)
    {
        printf("test_uint32_t_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_uint32_t_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %u, observed: %u.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_uint32_t_not_equal(char *file, int32_t line, char *message,
                             uint32_t expected, uint32_t observed)
{
    if(NULL == file)
    {
        printf("test_uint32_t_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_uint32_t_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %u, observed: %u.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_uint32_t_zero(char *file, int32_t line, char *message,
                        uint32_t observed)
{
    test_uint32_t_equal(file, line, message, 0, observed);
}

void test_uint32_t_not_zero(char *file, int32_t line, char *message,
                            uint32_t observed)
{
    test_uint32_t_not_equal(file, line, message, 0, observed);
}

void test_int64_t_equal(char *file, int32_t line, char *message,
                        int64_t expected, int64_t observed)
{
    if(NULL == file)
    {
        printf("test_int64_t_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_int64_t_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %ld, observed: %ld.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_int64_t_not_equal(char *file, int32_t line, char *message,
                            int64_t expected, int64_t observed)
{
    if(NULL == file)
    {
        printf("test_int64_t_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_int64_t_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %ld, observed: %ld.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_int64_t_zero(char *file, int32_t line, char *message,
                       int64_t observed)
{
    test_int64_t_equal(file, line, message, 0, observed);
}

void test_int64_t_not_zero(char *file, int32_t line, char *message,
                           int64_t observed)
{
    test_int64_t_not_equal(file, line, message, 0, observed);
}

void test_uint64_t_equal(char *file, int32_t line, char *message,
                        uint64_t expected, uint64_t observed)
{
    if(NULL == file)
    {
        printf("test_uint64_t_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_uint64_t_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %ld, observed: %ld.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_uint64_t_not_equal(char *file, int32_t line, char *message,
                            uint64_t expected, uint64_t observed)
{
    if(NULL == file)
    {
        printf("test_uint64_t_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_uint64_t_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %ld, observed: %ld.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_uint64_t_zero(char *file, int32_t line, char *message,
                       uint64_t observed)
{
    test_uint64_t_equal(file, line, message, 0, observed);
}

void test_uint64_t_not_zero(char *file, int32_t line, char *message,
                           uint64_t observed)
{
    test_uint64_t_not_equal(file, line, message, 0, observed);
}

void test_ptr_equal(char *file, int32_t line, char *message, void *expected,
                    void *observed)
{
    if(NULL == file)
    {
        printf("test_ptr_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_ptr_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected!=observed)
    {
        printf("%s (%d) %s expected: %p, observed: %p.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_ptr_not_equal(char *file, int32_t line, char *message, void *expected,
                        void *observed)
{
    if(NULL == file)
    {
        printf("test_ptr_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_ptr_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %p, observed: %p.\n",
            file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_ptr_null(char *file, int32_t line, char *message, void *observed)
{
    test_ptr_equal(file, line, message, NULL, observed);
}

void test_ptr_not_null(char *file, int32_t line, char *message, void *observed)
{
    test_ptr_not_equal(file, line, message, NULL, observed);
}

void test_str_equal(char *file, int32_t line, char *message, char *expected,
                    char *observed)
{
    if(NULL == file)
    {
        printf("test_str_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_str_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(NULL == expected)
    {
        printf("test_str_equal(): NULL pointer to expected.\n");
        fail_test(1);
    }
    else if(NULL==observed)
    {
        printf("test_str_equal(): NULL pointer to observed.\n");
        fail_test(1);
    }
    else if(strcmp(expected, observed))
    {
        printf("%s (%d) %s expected: \"%s\", observed: \"%s\"\n",
            file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_str_not_equal(char *file, int32_t line, char *message, char *expected,
                        char *observed)
{
    if(NULL == file)
    {
        printf("test_str_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_str_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(NULL == expected)
    {
        printf("test_str_not_equal(): NULL pointer to expected.\n");
        fail_test(1);
    }
    else if(NULL == observed)
    {
        printf("test_str_not_equal(): NULL pointer to observed.\n");
        fail_test(1);
    }
    else if(!strcmp(expected, observed))
    {
        printf("%s (%d) %s expected: \"%s\", observed: \"%s\"\n",
            file, line, message, expected, observed);
        fail_test(1);
    }
    else 
    {
        pass_test();
    }
}

void test_str_empty(char *file, int32_t line, char *message, char *observed)
{
    if(NULL == file)
    {
        printf("test_str_empty(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_str_empty(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(NULL == observed)
    {
        printf("test_str_empty(): NULL pointer to observed.\n");
        fail_test(1);
    }
    else if(strlen(observed)>0)
    {
        printf("%s (%d) %s observed: \"%s\"\n",
            file, line, message, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_str_not_empty(char *file, int32_t line, char *message, char *observed)
{
    if(NULL == file)
    {
        printf("test_str_not_empty(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_str_not_empty(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(NULL == observed)
    {
        printf("test_str_not_empty(): NULL pointer to observed.\n");
        fail_test(1);
    }
    else if(strlen(observed)==0)
    {
        printf("%s (%d) %s observed: \"%s\"\n",
            file, line, message, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_size_t_equal(char *file, int32_t line, char *message,
                       size_t expected, size_t observed)
{
    if(NULL == file)
    {
        printf("test_size_t_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_size_t_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %u, observed: %u.\n",
            file, line, message, (unsigned int)expected,
            (unsigned int)observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_size_t_not_equal(char *file, int32_t line, char *message,
            size_t expected, size_t observed)
{
    if(NULL == file)
    {
        printf("test_size_t_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_size_t_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %u, observed: %u.\n",
               file, line, message, (unsigned int)expected, (unsigned int)observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_size_t_zero(char *file, int32_t line, char *message,
                      size_t observed)
{
    test_size_t_equal(file, line, message, 0, observed);
}

void test_size_t_not_zero(char *file, int32_t line, char *message,
                          size_t observed)
{
    test_size_t_not_equal(file, line, message, 0, observed);
}

void test_float_equal(char *file, int32_t line, char *message,
                      float expected, float observed)
{
    if(NULL == file)
    {
        printf("test_float_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_float_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %f, observed: %f.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_float_not_equal(char *file, int32_t line, char *message,
                          float expected, float observed)
{
    if(NULL == file)
    {
        printf("test_float_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_float_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %f, observed: %f.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_float_zero(char *file, int32_t line, char *message,
                     float observed)
{
    test_float_equal(file, line, message, 0, observed);
}

void test_float_not_zero(char *file, int32_t line, char *message,
                         float observed)
{
    test_float_not_equal(file, line, message, 0, observed);
}

void test_float_in_range(char *file, int32_t line, char *message, float low,
                         float high, float observed)
{
    if(NULL == file)
    {
        printf("test_float_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_float_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if((observed < low) || (observed > high))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, low, high, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_float_not_in_range(char *file, int32_t line, char *message, float low,
                             float high, float observed)
{
    if(NULL == file)
    {
        printf("test_float_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_float_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if( ! ((observed < low) || (observed > high)))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, low, high, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_float_approx_equal(char *file, int32_t line, char *message, float expected,
                             float tolerance, float observed)
{
    if (NULL == file)
    {
        printf("test_float_approx_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if (NULL == message)
    {
        printf("test_float_approx_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if (tolerance < 0.0)
    {
        printf("test_float_approx_equal(): tolerance %g too low.\n", tolerance);
        fail_test(1);
    }
    else if (tolerance >= 1.0)
    {
        printf("test_float_approx_equal(): tolerance %g too high.\n", tolerance);
        fail_test(1);
    }
    else if ( (observed < ((1.0 - tolerance) * expected)) || (observed > ((1.0 + tolerance) * expected)))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, ((1.0 - tolerance) * expected), ((1.0 + tolerance) * expected), observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_float_approx_not_equal(char *file, int32_t line, char *message, float expected,
                                 float tolerance, float observed)
{
    if (NULL == file)
    {
        printf("test_float_approx_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if (NULL == message)
    {
        printf("test_float_approx_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if (tolerance < 0.0)
    {
        printf("test_float_approx_equal(): tolerance %g too low.\n", tolerance);
        fail_test(1);
    }
    else if (tolerance >= 1.0)
    {
        printf("test_float_approx_equal(): tolerance %g too high.\n", tolerance);
        fail_test(1);
    }
    else if ( ! ( (observed < ((1.0 - tolerance) * expected)) || (observed > ((1.0 + tolerance) * expected))))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, ((1.0 - tolerance) * expected), ((1.0 + tolerance) * expected), observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_equal(char *file, int32_t line, char *message,
                       double expected, double observed)
{
    if(NULL == file)
    {
        printf("test_double_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_double_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %g, observed: %g.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_not_equal(char *file, int32_t line, char *message,
                           double expected, double observed)
{
    if(NULL == file)
    {
        printf("test_double_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_double_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %g, observed: %g.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_zero(char *file, int32_t line, char *message,
                      double observed)
{
    test_double_equal(file, line, message, 0, observed);
}

void test_double_not_zero(char *file, int32_t line, char *message,
                          double observed)
{
    test_double_not_equal(file, line, message, 0, observed);
}

void test_double_in_range(char *file, int32_t line, char *message, double low,
                          double high, double observed)
{
    if(NULL == file)
    {
        printf("test_double_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_double_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if((observed < low) || (observed > high))
    {
        printf("%s (%d) %s low: %g, high: %g, observed: %g.\n",
               file, line, message, low, high, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_not_in_range(char *file, int32_t line, char *message, double low,
                              double high, double observed)
{
    if(NULL == file)
    {
        printf("test_double_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_double_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if( ! ((observed < low) || (observed > high)))
    {
        printf("%s (%d) %s low: %g, high: %g, observed: %g.\n",
               file, line, message, low, high, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_approx_equal(char *file, int32_t line, char *message, double expected,
                             double tolerance, double observed)
{
    if (NULL == file)
    {
        printf("test_double_approx_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if (NULL == message)
    {
        printf("test_double_approx_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if (tolerance < 0.0)
    {
        printf("test_double_approx_equal(): tolerance %g too low.\n", tolerance);
        fail_test(1);
    }
    else if (tolerance >= 1.0)
    {
        printf("test_double_approx_equal(): tolerance %g too high.\n", tolerance);
        fail_test(1);
    }
    else if ( (observed < ((1.0 - tolerance) * expected)) || (observed > ((1.0 + tolerance) * expected)))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, ((1.0 - tolerance) * expected), ((1.0 + tolerance) * expected), observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_double_approx_not_equal(char *file, int32_t line, char *message, double expected,
                                 double tolerance, double observed)
{
    if (NULL == file)
    {
        printf("test_double_approx_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if (NULL == message)
    {
        printf("test_double_approx_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if (tolerance < 0.0)
    {
        printf("test_double_approx_equal(): tolerance %g too low.\n", tolerance);
        fail_test(1);
    }
    else if (tolerance >= 1.0)
    {
        printf("test_double_approx_equal(): tolerance %g too high.\n", tolerance);
        fail_test(1);
    }
    else if ( ! ( (observed < ((1.0 - tolerance) * expected)) || (observed > ((1.0 + tolerance) * expected))))
    {
        printf("%s (%d) %s low: %f, high: %f, observed: %f.\n",
               file, line, message, ((1.0 - tolerance) * expected), ((1.0 + tolerance) * expected), observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}


void test_set_do_not_stop_on(void)
{
    do_not_stop = 1;
}

void test_set_do_not_stop_off(void)
{
    do_not_stop = 0;
}

int32_t test_get_do_not_stop(void)
{
    return do_not_stop;
}

void test_set_fail_callback(void (*callback)(void))
{
    fail_callback = callback;
}

void test_set_pass_callback(void (*callback)(void))
{
    pass_callback = callback;
}

int32_t test_get_pass_count(void)
{
    return pass_count;
}

int32_t test_get_fail_count(void)
{
    return fail_count;
}

void reset_pass_count(void)
{
    pass_count = 0;
}

void reset_fail_count(void)
{
    fail_count = 0;
}

void test_fixed_q31_equal(char *file, int32_t line, char *message,
                          int32_t expected, int32_t observed)
{
    if(NULL == file)
    {
        printf("test_fixed_q31_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_fixed_q31_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected != observed)
    {
        printf("%s (%d) %s expected: %d, observed: %d.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_fixed_q31_not_equal(char *file, int32_t line, char *message,
                              int32_t expected, int32_t observed)
{
    if(NULL == file)
    {
        printf("test_fixed_q31_not_equal(): NULL pointer to file.\n");
        fail_test(1);
    }
    else if(NULL == message)
    {
        printf("test_fixed_q31_not_equal(): NULL pointer to message.\n");
        fail_test(1);
    }
    else if(expected == observed)
    {
        printf("%s (%d) %s expected: %d, observed: %d.\n",
               file, line, message, expected, observed);
        fail_test(1);
    }
    else
    {
        pass_test();
    }
}

void test_fixed_q31_zero(char *file, int32_t line, char *message,
                         int32_t observed)
{
    test_fixed_q31_equal(file, line, message, 0, observed);
}

void test_fixed_q31_not_zero(char *file, int32_t line, char *message,
                             int32_t observed)
{
    test_fixed_q31_not_equal(file, line, message, 0, observed);
}

