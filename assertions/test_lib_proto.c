/*****************************************************************************
 * Prototype code for unit test library.
 * Sat May 28 12:42:41 BST 2011
 * Copyright (C) 2011-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2011-05-29: setup() and teardown().
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct
{
	int32_t verbose_output;
}
user_options_t;

void usage(void);
int32_t get_options(int argc, char *argv[], user_options_t *user_options);
int32_t run_tests(user_options_t *user_options);

void setup(void);
void teardown(void);

void foo_test(void);
void bar_test(void);

int main(int argc, char *argv[])
{
	user_options_t user_options={0};
	int32_t tests_executed=0;

	get_options(argc, argv, &user_options);

	if(1==user_options.verbose_output)
	{
		printf("Verbose output selected.\n");
	}

	tests_executed=run_tests(&user_options);
	printf("Tests executed: %d\n", tests_executed);

	return 0;
}

void usage(void)
{
	printf("Banana!\n");
}

int32_t get_options(int argc, char *argv[], user_options_t *user_options)
{
	int32_t i;

	for(i=1; i<argc; i++)
	{
		printf("argv[%d]: %s\n", i, argv[i]);
		if(!strcmp(argv[i], "-h"))
		{
			usage();
			exit(1);
		}
		else if(!strcmp(argv[i], "--help"))
		{
			usage();
			exit(1);
		}
		else if(!strcmp(argv[i], "-v"))
		{
			user_options->verbose_output=1;
		}
		else if(!strcmp(argv[i], "--verbose"))
		{
			user_options->verbose_output=1;
		}
		else
		{
			printf("Unrecognised command line option: %s.\n", argv[i]);
			printf("Try %s -h (or --help) for help.\n", argv[0]);
			exit(1);
		}
	}

	return 0;
}

int32_t run_tests(user_options_t *user_options)
{
	int32_t tests_executed=0;

	printf("run_tests()\n");

	setup();

	if(user_options->verbose_output!=0)
	{
		printf("foo_test\n");
	}
	foo_test();
	if(user_options->verbose_output!=0)
	{
		printf("\n");
	}
	else
	{
		printf(".");
	}
	tests_executed++;

	if(user_options->verbose_output!=0)
	{
		printf("bar_test\n");
	}
	bar_test();
	if(user_options->verbose_output!=0)
	{
		printf("\n");
	}
	else
	{
		printf(".");
	}
	tests_executed++;

	teardown();

	printf("\n");

	return tests_executed;
}

void setup(void)
{
}

void teardown(void)
{
}


void foo_test(void)
{
}

void bar_test(void)
{
}
