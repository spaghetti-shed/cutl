/******************************************************************************
 * typeindex_test - Associate C data types with cutl test assertion macros
 *                  (test).
 * Fri Jun 10 22:14:39 BST 2022
 * Copyright (C) 2022-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2022-06-10 Initial creation.
 * 2022-06-11 Add remaining data types except strings.
 * 2024-09-26 Add the new float and double approximations.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "assertions.h"
#include "typeindex.h"

void test_int32_macros(void);
void test_uint32_macros(void);
void test_int64_macros(void);
void test_uint64_macros(void);
void test_ptr_macros(void);
void test_size_t_macros(void);
void test_float_macros(void);
void test_double_macros(void);
void test_fixed_q31_macros(void);
void test_str_to_condition(void);

int main(int argc, char *argv[])
{
    printf("typeindex test.\n");

    test_int32_macros();
    test_uint32_macros();
    test_int64_macros();
    test_uint64_macros();
    test_ptr_macros();
    /* No strings for now. */
    test_size_t_macros();
    test_float_macros();
    test_double_macros();
    test_fixed_q31_macros();
    test_str_to_condition();

    return 0;
}

void test_int32_macros(void)
{
    TEST_STR_EQUAL("int32_t ZERO",      "TEST_INT32_ZERO",      cutl_macro_get("int32_t", ZERO));
    TEST_STR_EQUAL("int32_t NOT_ZERO",  "TEST_INT32_NOT_ZERO",  cutl_macro_get("int32_t", NOT_ZERO));
    TEST_STR_EQUAL("int32_t EQUAL",     "TEST_INT32_EQUAL",     cutl_macro_get("int32_t", EQUAL));
    TEST_STR_EQUAL("int32_t NOT_EQUAL", "TEST_INT32_NOT_EQUAL", cutl_macro_get("int32_t", NOT_EQUAL));
}

void test_uint32_macros(void)
{
    TEST_STR_EQUAL("uint32_t ZERO",      "TEST_UINT32_ZERO",      cutl_macro_get("uint32_t", ZERO));
    TEST_STR_EQUAL("uint32_t NOT_ZERO",  "TEST_UINT32_NOT_ZERO",  cutl_macro_get("uint32_t", NOT_ZERO));
    TEST_STR_EQUAL("uint32_t EQUAL",     "TEST_UINT32_EQUAL",     cutl_macro_get("uint32_t", EQUAL));
    TEST_STR_EQUAL("uint32_t NOT_EQUAL", "TEST_UINT32_NOT_EQUAL", cutl_macro_get("uint32_t", NOT_EQUAL));
}

void test_int64_macros(void)
{
    TEST_STR_EQUAL("int64_t ZERO",      "TEST_INT64_ZERO",      cutl_macro_get("int64_t", ZERO));
    TEST_STR_EQUAL("int64_t NOT_ZERO",  "TEST_INT64_NOT_ZERO",  cutl_macro_get("int64_t", NOT_ZERO));
    TEST_STR_EQUAL("int64_t EQUAL",     "TEST_INT64_EQUAL",     cutl_macro_get("int64_t", EQUAL));
    TEST_STR_EQUAL("int64_t NOT_EQUAL", "TEST_INT64_NOT_EQUAL", cutl_macro_get("int64_t", NOT_EQUAL));
}

void test_uint64_macros(void)
{
    TEST_STR_EQUAL("uint64_t ZERO",      "TEST_UINT64_ZERO",      cutl_macro_get("uint64_t", ZERO));
    TEST_STR_EQUAL("uint64_t NOT_ZERO",  "TEST_UINT64_NOT_ZERO",  cutl_macro_get("uint64_t", NOT_ZERO));
    TEST_STR_EQUAL("uint64_t EQUAL",     "TEST_UINT64_EQUAL",     cutl_macro_get("uint64_t", EQUAL));
    TEST_STR_EQUAL("uint64_t NOT_EQUAL", "TEST_UINT64_NOT_EQUAL", cutl_macro_get("uint64_t", NOT_EQUAL));
}

void test_ptr_macros(void)
{
    TEST_STR_EQUAL("ptr NULL",      "TEST_PTR_NULL",      cutl_macro_get("ptr *", PTR_NULL));
    TEST_STR_EQUAL("ptr NOT_NULL",  "TEST_PTR_NOT_NULL",  cutl_macro_get("ptr *" ,NOT_NULL));
    TEST_STR_EQUAL("ptr EQUAL",     "TEST_PTR_EQUAL",     cutl_macro_get("ptr *", EQUAL));
    TEST_STR_EQUAL("ptr NOT_EQUAL", "TEST_PTR_NOT_EQUAL", cutl_macro_get("ptr *", NOT_EQUAL));
}

void test_size_t_macros(void)
{
    TEST_STR_EQUAL("size_t_t ZERO",      "TEST_SIZE_T_ZERO",      cutl_macro_get("size_t", ZERO));
    TEST_STR_EQUAL("size_t_t NOT_ZERO",  "TEST_SIZE_T_NOT_ZERO",  cutl_macro_get("size_t", NOT_ZERO));
    TEST_STR_EQUAL("size_t_t EQUAL",     "TEST_SIZE_T_EQUAL",     cutl_macro_get("size_t", EQUAL));
    TEST_STR_EQUAL("size_t_t NOT_EQUAL", "TEST_SIZE_T_NOT_EQUAL", cutl_macro_get("size_t", NOT_EQUAL));
}

void test_float_macros(void)
{
    TEST_STR_EQUAL("float ZERO",             "TEST_FLOAT_ZERO",             cutl_macro_get("float", ZERO));
    TEST_STR_EQUAL("float NOT_ZERO",         "TEST_FLOAT_NOT_ZERO",         cutl_macro_get("float", NOT_ZERO));
    TEST_STR_EQUAL("float EQUAL",            "TEST_FLOAT_EQUAL",            cutl_macro_get("float", EQUAL));
    TEST_STR_EQUAL("float NOT_EQUAL",        "TEST_FLOAT_NOT_EQUAL",        cutl_macro_get("float", NOT_EQUAL));
    TEST_STR_EQUAL("float IN_RANGE",         "TEST_FLOAT_IN_RANGE",         cutl_macro_get("float", IN_RANGE));
    TEST_STR_EQUAL("float NOT_IN_RANGE",     "TEST_FLOAT_NOT_IN_RANGE",     cutl_macro_get("float", NOT_IN_RANGE));
    TEST_STR_EQUAL("float APPROX_EQUAL",     "TEST_FLOAT_APPROX_EQUAL",     cutl_macro_get("float", APPROX_EQUAL));
    TEST_STR_EQUAL("float APPROX_NOT_EQUAL", "TEST_FLOAT_APPROX_NOT_EQUAL", cutl_macro_get("float", APPROX_NOT_EQUAL));
}

void test_double_macros(void)
{
    TEST_STR_EQUAL("double ZERO",             "TEST_DOUBLE_ZERO",             cutl_macro_get("double", ZERO));
    TEST_STR_EQUAL("double NOT_ZERO",         "TEST_DOUBLE_NOT_ZERO",         cutl_macro_get("double", NOT_ZERO));
    TEST_STR_EQUAL("double EQUAL",            "TEST_DOUBLE_EQUAL",            cutl_macro_get("double", EQUAL));
    TEST_STR_EQUAL("double NOT_EQUAL",        "TEST_DOUBLE_NOT_EQUAL",        cutl_macro_get("double", NOT_EQUAL));
    TEST_STR_EQUAL("double IN_RANGE",         "TEST_DOUBLE_IN_RANGE",         cutl_macro_get("double", IN_RANGE));
    TEST_STR_EQUAL("double NOT_IN_RANGE",     "TEST_DOUBLE_NOT_IN_RANGE",     cutl_macro_get("double", NOT_IN_RANGE));
    TEST_STR_EQUAL("double APPROX_EQUAL",     "TEST_DOUBLE_APPROX_EQUAL",     cutl_macro_get("double", APPROX_EQUAL));
    TEST_STR_EQUAL("double APPROX_NOT_EQUAL", "TEST_DOUBLE_APPROX_NOT_EQUAL", cutl_macro_get("double", APPROX_NOT_EQUAL));
}

void test_fixed_q31_macros(void)
{
    TEST_STR_EQUAL("fixed_q31_t ZERO",      "TEST_FIXED_Q31_ZERO",      cutl_macro_get("fixed_q31_t", ZERO));
    TEST_STR_EQUAL("fixed_q31_t NOT_ZERO",  "TEST_FIXED_Q31_NOT_ZERO",  cutl_macro_get("fixed_q31_t", NOT_ZERO));
    TEST_STR_EQUAL("fixed_q31_t EQUAL",     "TEST_FIXED_Q31_EQUAL",     cutl_macro_get("fixed_q31_t", EQUAL));
    TEST_STR_EQUAL("fixed_q31_t NOT_EQUAL", "TEST_FIXED_Q31_NOT_EQUAL", cutl_macro_get("fixed_q31_t", NOT_EQUAL));
}

void test_str_to_condition(void)
{
    cutl_condition_t zero = ZERO;
    cutl_condition_t not_zero = NOT_ZERO;
    cutl_condition_t equal = EQUAL;
    cutl_condition_t not_equal = NOT_EQUAL;
    cutl_condition_t in_range = IN_RANGE;
    cutl_condition_t not_in_range = NOT_IN_RANGE;
    cutl_condition_t empty = EMPTY;
    cutl_condition_t not_empty = NOT_EMPTY;
    cutl_condition_t ptr_null = PTR_NULL;
    cutl_condition_t not_null = NOT_NULL;

    TEST_INT32_EQUAL("ZERO", zero, cutl_macro_str_to_condition("ZERO"));
    TEST_INT32_EQUAL("NOT_ZERO", not_zero, cutl_macro_str_to_condition("NOT_ZERO"));

    TEST_INT32_EQUAL("EQUAL", equal, cutl_macro_str_to_condition("EQUAL"));
    TEST_INT32_EQUAL("NOT_EQUAL", not_equal, cutl_macro_str_to_condition("NOT_EQUAL"));

    TEST_INT32_EQUAL("IN_RANGE", in_range, cutl_macro_str_to_condition("IN_RANGE"));
    TEST_INT32_EQUAL("NOT_IN_RANGE", not_in_range, cutl_macro_str_to_condition("NOT_IN_RANGE"));

    TEST_INT32_EQUAL("EMPTY", empty, cutl_macro_str_to_condition("EMPTY"));
    TEST_INT32_EQUAL("NOT_EMPTY", not_empty, cutl_macro_str_to_condition("NOT_EMPTY"));

    TEST_INT32_EQUAL("PTR_NULL", ptr_null, cutl_macro_str_to_condition("PTR_NULL"));
    TEST_INT32_EQUAL("NOT_NULL", not_null, cutl_macro_str_to_condition("NOT_NULL"));
}

