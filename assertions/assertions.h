/******************************************************************************
 * assertions (header)
 * Wed Nov 17 21:33:23 GMT 2010
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 17th November 2010 Taken from module_object.c.
 * 2011-04-22         Integers, pointers and strings. wrapped_exit().
 * 2013-05-31         size_t.
 * 2013-12-11         C++ externs.
 *                    Macros for floats.
 * 2014-02-09         Fix some types broken when doing float.
 * 2014-04-12         Add support for pass/fail callbacks and continuing on
 *                    failure of an assertion.
 * 2014-06-25         64-bit integers.
 * 2014-06-29         Unsigned 64-bit integers.
 * 2014-08-17         Fix types in 64-bit integer test functions.
 * 2014-11-27         Add accessors to reset pass and fail counts for
 *                    testing.
 * 2014-11-30         Make sure that the float assertions call the right
 *                    test functions.
 * 2014-12-01         Add floating point range tests.
 * 2015-01-06         Add double-precision floating point tests.
 * 2015-01-12         Add q31 fixed-point tests.
 * 2015-09-13         Add uint32_t tests.
 * 2024-09-26         Add macros for testing floats and doubles to within a
 *                    tolerance specified as a fraction of 1 e.g. 0.01.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __ASSERTIONS_H__
#define __ASSERTIONS_H__

#ifdef __cplusplus
extern "C"
{
#endif


#define TEST_OBSERVATION(message, condition, test) test_observation(__FILE__, __LINE__, message, condition, test)
#define TEST_INT32_EQUAL(message, condition, test) test_int32_t_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_INT32_NOT_EQUAL(message, condition, test) test_int32_t_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_INT32_ZERO(message, test) test_int32_t_zero(__FILE__, __LINE__, message, test)
#define TEST_INT32_NOT_ZERO(message, test) test_int32_t_not_zero(__FILE__, __LINE__, message, test)
#define TEST_UINT32_EQUAL(message, condition, test) test_uint32_t_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_UINT32_NOT_EQUAL(message, condition, test) test_uint32_t_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_UINT32_ZERO(message, test) test_uint32_t_zero(__FILE__, __LINE__, message, test)
#define TEST_UINT32_NOT_ZERO(message, test) test_uint32_t_not_zero(__FILE__, __LINE__, message, test)
#define TEST_INT64_EQUAL(message, condition, test) test_int64_t_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_INT64_NOT_EQUAL(message, condition, test) test_int64_t_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_INT64_ZERO(message, test) test_int64_t_zero(__FILE__, __LINE__, message, test)
#define TEST_INT64_NOT_ZERO(message, test) test_int64_t_not_zero(__FILE__, __LINE__, message, test)
#define TEST_UINT64_EQUAL(message, condition, test) test_uint64_t_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_UINT64_NOT_EQUAL(message, condition, test) test_uint64_t_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_UINT64_ZERO(message, test) test_uint64_t_zero(__FILE__, __LINE__, message, test)
#define TEST_UINT64_NOT_ZERO(message, test) test_uint64_t_not_zero(__FILE__, __LINE__, message, test)
#define TEST_PTR_EQUAL(message, condition, test) test_ptr_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_PTR_NOT_EQUAL(message, condition, test) test_ptr_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_PTR_NULL(message, test) test_ptr_null(__FILE__, __LINE__, message, test)
#define TEST_PTR_NOT_NULL(message, test) test_ptr_not_null(__FILE__, __LINE__, message, test)
#define TEST_STR_EQUAL(message, condition, test) test_str_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_STR_NOT_EQUAL(message, condition, test) test_str_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_STR_EMPTY(message, test) test_str_empty(__FILE__, __LINE__, message, test)
#define TEST_STR_NOT_EMPTY(message, test) test_str_not_empty(__FILE__, __LINE__, message, test)
#define TEST_SIZE_T_EQUAL(message, condition, test) test_size_t_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_SIZE_T_NOT_EQUAL(message, condition, test) test_size_t_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_SIZE_T_ZERO(message, test) test_size_t_zero(__FILE__, __LINE__, message, test)
#define TEST_SIZE_T_NOT_ZERO(message, test) test_size_t_not_zero(__FILE__, __LINE__, message, test)
#define TEST_FLOAT_EQUAL(message, condition, test) test_float_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_FLOAT_NOT_EQUAL(message, condition, test) test_float_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_FLOAT_ZERO(message, test) test_float_zero(__FILE__, __LINE__, message, test)
#define TEST_FLOAT_NOT_ZERO(message, test) test_float_not_zero(__FILE__, __LINE__, message, test)
#define TEST_FLOAT_IN_RANGE(message, low, high, test) test_float_in_range(__FILE__, __LINE__, message, low, high, test)
#define TEST_FLOAT_NOT_IN_RANGE(message, low, high, test) test_float_not_in_range(__FILE__, __LINE__, message, low, high, test)
#define TEST_FLOAT_APPROX_EQUAL(message, expected, tolerance, test) test_float_approx_equal(__FILE__, __LINE__, message, expected, tolerance, test)
#define TEST_FLOAT_APPROX_NOT_EQUAL(message, expected, tolerance, test) test_float_approx_not_equal(__FILE__, __LINE__, message, expected, tolerance, test)
#define TEST_DOUBLE_EQUAL(message, condition, test) test_double_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_DOUBLE_NOT_EQUAL(message, condition, test) test_double_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_DOUBLE_ZERO(message, test) test_double_zero(__FILE__, __LINE__, message, test)
#define TEST_DOUBLE_NOT_ZERO(message, test) test_double_not_zero(__FILE__, __LINE__, message, test)
#define TEST_DOUBLE_IN_RANGE(message, low, high, test) test_double_in_range(__FILE__, __LINE__, message, low, high, test)
#define TEST_DOUBLE_NOT_IN_RANGE(message, low, high, test) test_double_not_in_range(__FILE__, __LINE__, message, low, high, test)
#define TEST_DOUBLE_APPROX_EQUAL(message, expected, tolerance, test) test_double_approx_equal(__FILE__, __LINE__, message, expected, tolerance, test)
#define TEST_DOUBLE_APPROX_NOT_EQUAL(message, expected, tolerance, test) test_double_approx_not_equal(__FILE__, __LINE__, message, expected, tolerance, test)
#define TEST_FIXED_Q31_EQUAL(message, condition, test) test_fixed_q31_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_FIXED_Q31_NOT_EQUAL(message, condition, test) test_fixed_q31_not_equal(__FILE__, __LINE__, message, condition, test)
#define TEST_FIXED_Q31_ZERO(message, test) test_fixed_q31_zero(__FILE__, __LINE__, message, test)
#define TEST_FIXED_Q31_NOT_ZERO(message, test) test_fixed_q31_not_zero(__FILE__, __LINE__, message, test)

void test_observation(char *file, int32_t line, char *message, int32_t expected,
                      int32_t observed);

void test_int32_t_equal(char *file, int32_t line, char *message,
                        int32_t expected, int32_t observed);
void test_int32_t_not_equal(char *file, int32_t line, char *message,
                            int32_t expected, int32_t observed);
void test_int32_t_zero(char *file, int32_t line, char *message,
                       int32_t observed);
void test_int32_t_not_zero(char *file, int32_t line, char *message,
                           int32_t observed);
void test_uint32_t_equal(char *file, int32_t line, char *message,
                         uint32_t expected, uint32_t observed);
void test_uint32_t_not_equal(char *file, int32_t line, char *message,
                             uint32_t expected, uint32_t observed);
void test_uint32_t_zero(char *file, int32_t line, char *message,
                        uint32_t observed);
void test_uint32_t_not_zero(char *file, int32_t line, char *message,
                            uint32_t observed);
void test_int64_t_equal(char *file, int32_t line, char *message,
                        int64_t expected, int64_t observed);
void test_int64_t_not_equal(char *file, int32_t line, char *message,
                            int64_t expected, int64_t observed);
void test_int64_t_zero(char *file, int32_t line, char *message,
                       int64_t observed);
void test_int64_t_not_zero(char *file, int32_t line, char *message,
                           int64_t observed);
void test_uint64_t_equal(char *file, int32_t line, char *message,
                         uint64_t expected, uint64_t observed);
void test_uint64_t_not_equal(char *file, int32_t line, char *message,
                             uint64_t expected, uint64_t observed);
void test_uint64_t_zero(char *file, int32_t line, char *message,
                        uint64_t observed);
void test_uint64_t_not_zero(char *file, int32_t line, char *message,
                            uint64_t observed);
void test_ptr_equal(char *file, int32_t line, char *message, void *expected,
                    void *observed);
void test_ptr_not_equal(char *file, int32_t line, char *message, void *expected,
                        void *observed);
void test_ptr_null(char *file, int32_t line, char *message, void *observed);
void test_ptr_not_null(char *file, int32_t line, char *message, void *observed);
void test_str_equal(char *file, int32_t line, char *message, char *expected,
                    char *observed);
void test_str_not_equal(char *file, int32_t line, char *message, char *expected,
                        char *observed);
void test_str_empty(char *file, int32_t line, char *message, char *observed);
void test_str_not_empty(char *file, int32_t line, char *message, char *observed);

void test_size_t_equal(char *file, int32_t line, char *message,
                       size_t expected, size_t observed);
void test_size_t_not_equal(char *file, int32_t line, char *message,
                           size_t expected, size_t observed);
void test_size_t_zero(char *file, int32_t line, char *message,
                      size_t observed);
void test_size_t_not_zero(char *file, int32_t line, char *message,
                          size_t observed);
void test_float_equal(char *file, int32_t line, char *message,
                      float expected, float observed);
void test_float_not_equal(char *file, int32_t line, char *message,
                          float expected, float observed);
void test_float_zero(char *file, int32_t line, char *message,
                     float observed);
void test_float_not_zero(char *file, int32_t line, char *message,
                         float observed);
void test_float_in_range(char *file, int32_t line, char *message, float low, float high, float observed);
void test_float_not_in_range(char *file, int32_t line, char *message, float low, float high, float observed);
void test_float_approx_equal(char *file, int32_t line, char *message, float expected, float tolerance, float observed);
void test_float_approx_not_equal(char *file, int32_t line, char *message, float expected, float tolerance, float observed);

void test_double_equal(char *file, int32_t line, char *message,
                       double expected, double observed);
void test_double_not_equal(char *file, int32_t line, char *message,
                           double expected, double observed);
void test_double_zero(char *file, int32_t line, char *message,
                      double observed);
void test_double_not_zero(char *file, int32_t line, char *message,
                          double observed);
void test_double_in_range(char *file, int32_t line, char *message, double low, double high, double observed);
void test_double_not_in_range(char *file, int32_t line, char *message, double low, double high, double observed);
void test_double_approx_equal(char *file, int32_t line, char *message, double expected, double tolerance, double observed);
void test_double_approx_not_equal(char *file, int32_t line, char *message, double expected, double tolerance, double observed);

void test_fixed_q31_equal(char *file, int32_t line, char *message,
                          int32_t expected, int32_t observed);
void test_fixed_q31_not_equal(char *file, int32_t line, char *message,
                              int32_t expected, int32_t observed);
void test_fixed_q31_zero(char *file, int32_t line, char *message,
                         int32_t observed);
void test_fixed_q31_not_zero(char *file, int32_t line, char *message,
                             int32_t observed);


#ifdef TESTING
void wrapped_exit_reset_call_count(void);
int32_t wrapped_exit_get_call_count(void);
void reset_pass_count(void);
void reset_fail_count(void);
#endif

void    test_set_do_not_stop_on(void);
void    test_set_do_not_stop_off(void);
int32_t test_get_do_not_stop(void);
void    test_set_fail_callback(void (*callback)(void));
void    test_set_pass_callback(void (*callback)(void));
int32_t test_get_pass_count(void);
int32_t test_get_fail_count(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __ASSERTIONS_H__ */
