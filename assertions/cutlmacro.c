/******************************************************************************
 * cutlmacro - Utility to return cutl test macro for data type and test.
 * Sat Jun 11 14:13:45 BST 2022
 * Copyright (C) 2022-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2022-06-11 Initial creation.
 * 2024-09-26 Add the new float and double approximations.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "typeindex.h"

void usage(void)
{
    printf("cutlmacro: Given a C data type and test type, print the name of the appropriate cutl unit test macro or \"unknown\".\n");
    printf("\n");
    printf("Example\n");
    printf("  cutlmacro int32_t EQUAL\n");
    printf("\n");
    printf("Output:\n");
    printf("  TEST_INT32_EQUAL\n");
    printf("\n");
    printf("Returns:\n");
    printf("  0 on success.\n");
    printf("  EXIT_FAILRE on failure.\n");
    printf("\n");
    printf("All macros:\n");
    printf("\n");
    cutl_macro_types_dump();
    printf("\n");
}

int main(int argc, char *argv[])
{
    int32_t retcode = 0;
    int32_t index;

    if (argc < 3)
    {
        usage();
        exit(EXIT_FAILURE);
    }

    if (( ! strcmp(argv[1], "-h")) || ( ! strcmp(argv[1], "--help")))
    {
        usage();
        exit(EXIT_FAILURE);
    }

    for (index = 1; index < (argc - 1); index += 2)
    {
        printf("%s\n", cutl_macro_get(argv[index],
                       cutl_macro_str_to_condition(argv[index + 1])));
    }

    if (index == (argc - 1))
    {
        printf("Unprocessed argument: %s\n", argv[index]);
    }

    return retcode;
}

