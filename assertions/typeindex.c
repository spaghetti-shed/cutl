/******************************************************************************
 * typeindex - Associate C data types with cutl test assertion macros.
 * Fri Jun 10 22:14:39 BST 2022
 * Copyright (C) 2022-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2022-06-10 Initial creation.
 * 2022-06-11 Add remaining data types except strings.
 * 2024-09-26 Add the new float and double approximations.
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "typeindex.h"

typedef struct
{
    char *type;
    cutl_condition_t condition;
    char *macro;
}
macro_map_t;

macro_map_t macro_table[] =
{
    { "int32_t",     ZERO,             "TEST_INT32_ZERO"              },
    { "int32_t",     NOT_ZERO,         "TEST_INT32_NOT_ZERO"          },
    { "int32_t",     EQUAL,            "TEST_INT32_EQUAL"             },
    { "int32_t",     NOT_EQUAL,        "TEST_INT32_NOT_EQUAL"         },
    { "uint32_t",    ZERO,             "TEST_UINT32_ZERO"             },
    { "uint32_t",    NOT_ZERO,         "TEST_UINT32_NOT_ZERO"         },
    { "uint32_t",    EQUAL,            "TEST_UINT32_EQUAL"            },
    { "uint32_t",    NOT_EQUAL,        "TEST_UINT32_NOT_EQUAL"        },
    { "int64_t",     ZERO,             "TEST_INT64_ZERO"              },
    { "int64_t",     NOT_ZERO,         "TEST_INT64_NOT_ZERO"          },
    { "int64_t",     EQUAL,            "TEST_INT64_EQUAL"             },
    { "int64_t",     NOT_EQUAL,        "TEST_INT64_NOT_EQUAL"         },
    { "uint64_t",    ZERO,             "TEST_UINT64_ZERO"             },
    { "uint64_t",    NOT_ZERO,         "TEST_UINT64_NOT_ZERO"         },
    { "uint64_t",    EQUAL,            "TEST_UINT64_EQUAL"            },
    { "uint64_t",    NOT_EQUAL,        "TEST_UINT64_NOT_EQUAL"        },
    { "size_t",      ZERO,             "TEST_SIZE_T_ZERO"             },
    { "size_t",      NOT_ZERO,         "TEST_SIZE_T_NOT_ZERO"         },
    { "size_t",      EQUAL,            "TEST_SIZE_T_EQUAL"            },
    { "size_t",      NOT_EQUAL,        "TEST_SIZE_T_NOT_EQUAL"        },
    { "float",       ZERO,             "TEST_FLOAT_ZERO"              },
    { "float",       NOT_ZERO,         "TEST_FLOAT_NOT_ZERO"          },
    { "float",       EQUAL,            "TEST_FLOAT_EQUAL"             },
    { "float",       NOT_EQUAL,        "TEST_FLOAT_NOT_EQUAL"         },
    { "float",       IN_RANGE,         "TEST_FLOAT_IN_RANGE"          },
    { "float",       NOT_IN_RANGE,     "TEST_FLOAT_NOT_IN_RANGE"      },
    { "float",       APPROX_EQUAL,     "TEST_FLOAT_APPROX_EQUAL"      },
    { "float",       APPROX_NOT_EQUAL, "TEST_FLOAT_APPROX_NOT_EQUAL"  },
    { "double",      ZERO,             "TEST_DOUBLE_ZERO"             },
    { "double",      NOT_ZERO,         "TEST_DOUBLE_NOT_ZERO"         },
    { "double",      EQUAL,            "TEST_DOUBLE_EQUAL"            },
    { "double",      NOT_EQUAL,        "TEST_DOUBLE_NOT_EQUAL"        },
    { "double",      IN_RANGE,         "TEST_DOUBLE_IN_RANGE"         },
    { "double",      NOT_IN_RANGE,     "TEST_DOUBLE_NOT_IN_RANGE"     },
    { "double",      APPROX_EQUAL,     "TEST_DOUBLE_APPROX_EQUAL"     },
    { "double",      APPROX_NOT_EQUAL, "TEST_DOUBLE_APPROX_NOT_EQUAL" },
    { "fixed_q31_t", ZERO,             "TEST_FIXED_Q31_ZERO"          },
    { "fixed_q31_t", NOT_ZERO,         "TEST_FIXED_Q31_NOT_ZERO"      },
    { "fixed_q31_t", EQUAL,            "TEST_FIXED_Q31_EQUAL"         },
    { "fixed_q31_t", NOT_EQUAL,        "TEST_FIXED_Q31_NOT_EQUAL"     },
};

typedef struct
{
    cutl_condition_t condition;
    char *macro;
}
ptr_macro_map_t;

ptr_macro_map_t ptr_macro_table[] =
{
    { PTR_NULL,  "TEST_PTR_NULL"      },
    { NOT_NULL,  "TEST_PTR_NOT_NULL"  },
    { EQUAL,     "TEST_PTR_EQUAL"     },
    { NOT_EQUAL, "TEST_PTR_NOT_EQUAL" },
};

typedef struct
{
    char *name;
    cutl_condition_t condition;
}
condition_map_t;

condition_map_t condition_table[] =
{
    { "ZERO",         ZERO         },
    { "NOT_ZERO",     NOT_ZERO     },
    { "EQUAL",        EQUAL        },
    { "NOT_EQUAL",    NOT_EQUAL    },
    { "IN_RANGE",     IN_RANGE     },
    { "NOT_IN_RANGE", NOT_IN_RANGE },
    { "EMPTY",        EMPTY        },
    { "NOT_EMPTY",    NOT_EMPTY    },
    { "PTR_NULL",     PTR_NULL     },
    { "NOT_NULL",     NOT_NULL     },
};

#define NO_OF_ELEMENTS(array) (sizeof(array) / sizeof(array[0]))

char *cutl_macro_get(char *type, cutl_condition_t condition)
{
    char *result = "unknown";

    int32_t index;

    if (strlen(type)  < 1)
    {
    }
    else if ('*' == type[strlen(type) - 1])
    {
        for (index = 0; index < NO_OF_ELEMENTS(ptr_macro_table); index++)
        {
            if (ptr_macro_table[index].condition != condition)
            {
            }
            else
            {
                result = ptr_macro_table[index].macro;
                break;
            }
        }
    }
    else
    {
        for (index = 0; index < NO_OF_ELEMENTS(macro_table); index++)
        {
            if (strcmp(macro_table[index].type, type))
            {
            }
            else if (macro_table[index].condition != condition)
            {
            }
            else
            {
                result = macro_table[index].macro;
                break;
            }
        }
    }

    return result;
}

cutl_condition_t cutl_macro_str_to_condition(char *str)
{
    cutl_condition_t retcode = INVALID;
    int32_t index;

    for (index = 0; index < NO_OF_ELEMENTS(condition_table); index++)
    {
        if ( ! strcmp(str, condition_table[index].name))
        {
            retcode = condition_table[index].condition;
            break;
        }
    }

    return retcode;
}

void cutl_macro_types_dump(void)
{
    int32_t index;

    for (index = 0; index < NO_OF_ELEMENTS(macro_table); index++)
    {
        printf("%s\n", macro_table[index].macro);
    }
}
