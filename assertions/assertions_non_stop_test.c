/******************************************************************************
 * assertions_non_stop (test)
 * Sun Apr 13 13:38:57 BST 2014
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2014-04-13 Taken from assertions_test.c
 * 2014-06-25 64-bit integers.
 * 2014-06-29 Unsigned 64-bit integers.
 * 2014-11-27 Refactor.
 * 2014-12-01 Add floating point range tests.
 * 2015-01-06 Add double-precision floating point tests.
 * 2015-01-12 Add q31 fixed-point tests.
 * 2015-09-13 Add uint32_t tests.
 * 2024-09-25 Add float and double approximate (with 0.0 <= tolerance < 1.0)
 *            tests.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "assertions.h"

void test_observation_assertions(void);
void test_int32_assertions(void);
void test_uint32_assertions(void);
void test_int64_assertions(void);
void test_uint64_assertions(void);
void test_ptr_assertions(void);
void test_str_assertions(void);
void test_size_t_assertions(void);
void test_float_assertions(void);
void test_double_assertions(void);
void test_fixed_q31_assertions(void);

int main(int argc, char *argv[])
{
    printf("assertions non stop test.\n");

    test_observation_assertions();
    test_int32_assertions();
    test_uint32_assertions();
    test_int64_assertions();
    test_uint64_assertions();
    test_ptr_assertions();
    test_str_assertions();
    test_size_t_assertions();
    test_float_assertions();
    test_double_assertions();
    test_fixed_q31_assertions();

    return 0;
}

void test_observation_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_OBSERVATION("pass", 0, 0);
    exit_count = wrapped_exit_get_call_count();
    assert(0 == exit_count);

    TEST_OBSERVATION("pass", 1, 1);
    exit_count = wrapped_exit_get_call_count();
    assert(0 == exit_count);

    TEST_OBSERVATION("fail", 1, 0);
    exit_count = wrapped_exit_get_call_count();
    assert(0 == exit_count);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_int32_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_INT32_EQUAL("int32_t pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_INT32_EQUAL("int32_t fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_INT32_NOT_EQUAL("int32_t pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_INT32_NOT_EQUAL("int32_t fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_INT32_ZERO("int32_t pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_INT32_ZERO("int32_t fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_INT32_NOT_ZERO("int32_t pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_INT32_NOT_ZERO("int32_t fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_uint32_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_UINT32_EQUAL("uint32_t pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT32_EQUAL("uint32_t fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT32_NOT_EQUAL("uint32_t pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT32_NOT_EQUAL("uint32_t fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT32_ZERO("uint32_t pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT32_ZERO("uint32_t fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT32_NOT_ZERO("uint32_t pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT32_NOT_ZERO("uint32_t fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_ptr_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_PTR_EQUAL("ptr pass", (void *)0xDEADBEEF, (void *)0xDEADBEEF);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_PTR_EQUAL("ptr fail", (void *)0xDEADBEEF, (void *)0xBADDCAFE);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_PTR_NOT_EQUAL("ptr pass", (void *)0xDEADBEEF, (void *)0xBADDCAFE);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_PTR_NOT_EQUAL("ptr fail", (void *)0xDEADBEEF, (void *)0xDEADBEEF);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_PTR_NULL("ptr pass", NULL);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_PTR_NULL("ptr fail", (void *)0xDEADBEEF);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_PTR_NOT_NULL("ptr pass", (void *)0xDEADBEEF);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_PTR_NOT_NULL("ptr fail", NULL);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_str_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_STR_EQUAL("str pass", "hello", "hello");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_STR_EQUAL("str fail", "hello", "world");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_STR_NOT_EQUAL("str pass", "hello", "world");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_STR_NOT_EQUAL("str fail", "hello", "hello");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_STR_EMPTY("str pass", "");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_STR_EMPTY("str fail", "hello");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_STR_NOT_EMPTY("str pass", "hello");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_STR_NOT_EMPTY("str fail", "");
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_size_t_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_SIZE_T_EQUAL("size_t pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_SIZE_T_EQUAL("size_t fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_SIZE_T_NOT_EQUAL("size_t pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_SIZE_T_NOT_EQUAL("size_t fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_SIZE_T_ZERO("size_t pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_SIZE_T_ZERO("size_t fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_SIZE_T_NOT_ZERO("size_t pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_SIZE_T_NOT_ZERO("size_t fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_float_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_EQUAL("float pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_EQUAL("float fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_NOT_EQUAL("float pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_NOT_EQUAL("float fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_ZERO("float pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_ZERO("float fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_NOT_ZERO("float pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_NOT_ZERO("float fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_IN_RANGE("float pass", 1.0, 1.2, 1.1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_IN_RANGE("float pass", 1.0, 1.2, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_IN_RANGE("float pass", 1.0, 1.2, 1.2);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_IN_RANGE("float fail", 1.0, 1.2, 0.9);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_NOT_IN_RANGE("float pass", 1.0, 1.2, 0.9);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_NOT_IN_RANGE("float pass", 1.0, 1.2, 1.3);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_NOT_IN_RANGE("float fail", 1.0, 1.2, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_FLOAT_NOT_IN_RANGE("float fail", 1.0, 1.2, 1.1);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_FLOAT_NOT_IN_RANGE("float fail", 1.0, 1.2, 1.2);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_APPROX_EQUAL("float pass", 1.0, 0.101, 1.0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_APPROX_EQUAL("float pass", 1.0, 0.101, 0.9);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_APPROX_EQUAL("float pass", 1.0, 0.101, 1.1);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_APPROX_EQUAL("float fail", 1.0, 0.1, 0.9);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_FLOAT_APPROX_EQUAL("float fail", 1.0, 0.1, 1.1);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FLOAT_APPROX_NOT_EQUAL("float pass", 1.0, 0.1, 0.9);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_APPROX_NOT_EQUAL("float pass", 1.0, 0.1, 1.1);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FLOAT_APPROX_NOT_EQUAL("float fail", 1.0, 0.1, 0.91);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_FLOAT_APPROX_NOT_EQUAL("float fail", 1.0, 0.1, 1.09);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_FLOAT_APPROX_NOT_EQUAL("float fail", 1.0, 0.1, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);


    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_int64_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_INT64_EQUAL("int64_t pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_INT64_EQUAL("int64_t fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_INT64_NOT_EQUAL("int64_t pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_INT64_NOT_EQUAL("int64_t fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_INT64_ZERO("int64_t pass", 0);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_INT64_ZERO("int64_t fail", 1);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);

    TEST_INT64_NOT_ZERO("int64_t pass", 1);
    pass_count = test_get_pass_count();
    assert(4 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);

    TEST_INT64_NOT_ZERO("int64_t fail", 0);
    pass_count = test_get_pass_count();
    assert(4 == pass_count);
    fail_count = test_get_fail_count();
    assert(4 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_uint64_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_UINT64_EQUAL("uint64_t pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT64_EQUAL("uint64_t fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT64_NOT_EQUAL("uint64_t pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT64_NOT_EQUAL("uint64_t fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT64_ZERO("uint64_t pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT64_ZERO("uint64_t fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_UINT64_NOT_ZERO("uint64_t pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_UINT64_NOT_ZERO("uint64_t fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_double_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_EQUAL("double pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_EQUAL("double fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_NOT_EQUAL("double pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_NOT_EQUAL("double fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_ZERO("double pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_ZERO("double fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_NOT_ZERO("double pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_NOT_ZERO("double fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_IN_RANGE("double pass", 1.0, 1.2, 1.1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_IN_RANGE("double pass", 1.0, 1.2, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_IN_RANGE("double pass", 1.0, 1.2, 1.2);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_IN_RANGE("double fail", 1.0, 1.2, 0.9);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_NOT_IN_RANGE("double pass", 1.0, 1.2, 0.9);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_NOT_IN_RANGE("double pass", 1.0, 1.2, 1.3);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_NOT_IN_RANGE("double fail", 1.0, 1.2, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_DOUBLE_NOT_IN_RANGE("double fail", 1.0, 1.2, 1.1);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_DOUBLE_NOT_IN_RANGE("double fail", 1.0, 1.2, 1.2);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_APPROX_EQUAL("double pass", 1.0, 0.1, 1.0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_APPROX_EQUAL("double pass", 1.0, 0.1, 0.9);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_APPROX_EQUAL("double pass", 1.0, 0.1, 1.1);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_APPROX_EQUAL("double fail", 1.0, 0.1, 0.89);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_DOUBLE_APPROX_EQUAL("double fail", 1.0, 0.1, 1.11);
    pass_count = test_get_pass_count();
    assert(3 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_DOUBLE_APPROX_NOT_EQUAL("double pass", 1.0, 0.1, 0.89);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_APPROX_NOT_EQUAL("double pass", 1.0, 0.1, 1.11);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_DOUBLE_APPROX_NOT_EQUAL("double fail", 1.0, 0.1, 0.9);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    TEST_DOUBLE_APPROX_NOT_EQUAL("double fail", 1.0, 0.1, 1.1);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(2 == fail_count);

    TEST_DOUBLE_APPROX_NOT_EQUAL("double fail", 1.0, 0.1, 1.0);
    pass_count = test_get_pass_count();
    assert(2 == pass_count);
    fail_count = test_get_fail_count();
    assert(3 == fail_count);


    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

void test_fixed_q31_assertions(void)
{
    int32_t exit_count = 0;
    int32_t pass_count = 0;
    int32_t fail_count = 0;
    int32_t status;

    status = test_get_do_not_stop();
    assert(0 == status);

    test_set_do_not_stop_on();
    status = test_get_do_not_stop();
    assert(1 == status);

    wrapped_exit_reset_call_count();

    reset_pass_count();
    reset_fail_count();

    TEST_FIXED_Q31_EQUAL("fixed_q31 pass", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FIXED_Q31_EQUAL("fixed_q31 fail", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FIXED_Q31_NOT_EQUAL("fixed_q31 pass", 1, 2);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FIXED_Q31_NOT_EQUAL("fixed_q31 fail", 1, 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FIXED_Q31_ZERO("fixed_q31 pass", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FIXED_Q31_ZERO("fixed_q31 fail", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    reset_pass_count();
    reset_fail_count();

    TEST_FIXED_Q31_NOT_ZERO("fixed_q31 pass", 1);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(0 == fail_count);

    TEST_FIXED_Q31_NOT_ZERO("fixed_q31 fail", 0);
    pass_count = test_get_pass_count();
    assert(1 == pass_count);
    fail_count = test_get_fail_count();
    assert(1 == fail_count);

    exit_count=wrapped_exit_get_call_count();
    assert(0 == exit_count);

    test_set_do_not_stop_off();
    status = test_get_do_not_stop();
    assert(0 == status);
}

