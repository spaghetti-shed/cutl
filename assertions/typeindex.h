/******************************************************************************
 * typeindex - Associate C data types with cutl test assertion macros (header).
 * Fri Jun 10 22:14:39 BST 2022
 * Copyright (C) 2022-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2022-06-10 Initial creation.
 * 2022-06-11 Add remaining data types except strings.
 * 2024-09-26 Add new float and double approximations.
 ******************************************************************************/

#ifndef __TYPEINDEX_H__
#define __TYPEINDEX_H__

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum
{
    INVALID = -1,
    ZERO,
    NOT_ZERO,
    EQUAL,
    NOT_EQUAL,
    IN_RANGE,
    NOT_IN_RANGE,
    APPROX_EQUAL,
    APPROX_NOT_EQUAL,
    EMPTY,
    NOT_EMPTY,
    PTR_NULL,
    NOT_NULL
}
cutl_condition_t;

char *cutl_macro_get(char *type, cutl_condition_t condition);
cutl_condition_t cutl_macro_str_to_condition(char *str);

void cutl_macro_types_dump(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __TYPEINDEX_H__ */

