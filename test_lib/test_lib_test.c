/*****************************************************************************
 * Unit test library tests.
 * Tue Apr 15 22:18:14 BST 2014
 * Copyright (C) 2011-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2014-04-15: Initial creation.
 * 2014-04-21: Fix ordering of assignments.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "test_lib.h"
#include "../assertions/assertions.h"

void foo_test(void);
void bar_test(void);

cutl_test_case_t tests[] =
{
    { "foo_test", foo_test },
    { "bar_test", bar_test }
};

void test_run_all_test_cases(void);
void test_run_single_test_cases_foo_test_short(void);
void test_run_single_test_cases_bar_test_short(void);
void test_run_single_test_cases_foo_test_short_verbose(void);
void test_run_single_test_cases_bar_test_short_verbose(void);
void test_run_single_test_cases_foo_test_long(void);
void test_run_single_test_cases_bar_test_long(void);
void test_run_single_test_cases_foo_test_long_verbose(void);
void test_run_single_test_cases_bar_test_long_verbose(void);
void test_list_test_cases_short(void);
void test_list_test_cases_long(void);

int main(int argc, char *argv[])
{
    printf("test_lib tests.\n");

    test_run_all_test_cases();
    test_run_single_test_cases_foo_test_short();
    test_run_single_test_cases_bar_test_short();
    test_run_single_test_cases_foo_test_short_verbose();
    test_run_single_test_cases_bar_test_short_verbose();
    test_run_single_test_cases_foo_test_long();
    test_run_single_test_cases_bar_test_long();
    test_run_single_test_cases_foo_test_long_verbose();
    test_run_single_test_cases_bar_test_long_verbose();
    test_list_test_cases_short();
    test_list_test_cases_long();

    return 0;
}


void foo_test(void)
{
}

void bar_test(void)
{
}

void test_run_all_test_cases(void)
{
    char *argv[] = { "prog" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_get_options(argc, argv);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_ZERO("test_case", cutl_get_user_options_test_case());
}

void test_run_single_test_cases_foo_test_short(void)
{
    char *argv[] = { "prog", "-c", "foo_test" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 0, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_bar_test_short(void)
{
    char *argv[] = { "prog", "-c", "bar_test" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 1, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_foo_test_short_verbose(void)
{
    char *argv[] = { "prog", "-c", "foo_test", "-v" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_NOT_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 0, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_bar_test_short_verbose(void)
{
    char *argv[] = { "prog", "-c", "bar_test", "-v" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_NOT_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 1, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_foo_test_long(void)
{
    char *argv[] = { "prog", "--case", "foo_test" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 0, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_bar_test_long(void)
{
    char *argv[] = { "prog", "--case", "bar_test" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 1, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_foo_test_long_verbose(void)
{
    char *argv[] = { "prog", "--case", "foo_test", "--verbose" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_NOT_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 0, cutl_get_user_options_test_case());
}

void test_run_single_test_cases_bar_test_long_verbose(void)
{
    char *argv[] = { "prog", "--case", "bar_test", "--verbose" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_NOT_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_NOT_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_EQUAL("test_case", 1, cutl_get_user_options_test_case());
}

void test_list_test_cases_short(void)
{
    char *argv[] = { "prog", "-l" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_NOT_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_ZERO("test_case", cutl_get_user_options_test_case());
}

void test_list_test_cases_long(void)
{
    char *argv[] = { "prog", "--list" };
    int argc = NO_OF_ELEMENTS(argv);

    cutl_set_test_lib(NULL, NULL, 0);
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    TEST_INT32_ZERO("verbose_output", cutl_get_user_options_verbose_output());
    TEST_INT32_NOT_ZERO("list_test_cases", cutl_get_user_options_list_test_cases());
    TEST_INT32_ZERO("do_single_test_case", cutl_get_user_options_do_single_test_case());
    TEST_INT32_ZERO("test_case", cutl_get_user_options_test_case());
}

