/*****************************************************************************
 * Prototype code for unit test library.
 * Sat May 28 12:42:41 BST 2011
 * Copyright (C) 2011-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2011-05-29: setup() and teardown().
 * 2013-09-11: Hide the user options data structure.
 * 2014-02-09: Move the printf()s into test_lib.c.
 * 2014-04-12: Command line option to list test cases.
 *             API function to initialise test library.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "test_lib.h"

void foo_test(void);
void bar_test(void);

cutl_test_case_t tests[] =
{
    { "foo_test", foo_test },
    { "bar_test", bar_test }
};

int main(int argc, char *argv[])
{

#if 0
    cutl_set_test_array(tests, NO_OF_ELEMENTS(tests));
#endif
    cutl_set_test_lib("proto", tests, NO_OF_ELEMENTS(tests));
    cutl_get_options(argc, argv);

    cutl_run_tests();

    return 0;
}


void foo_test(void)
{
}

void bar_test(void)
{
}

