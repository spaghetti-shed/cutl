/*****************************************************************************
 * Unit test library.
 * Sat May 28 12:42:41 BST 2011
 * Copyright (C) 2011-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2011-05-29: setup() and teardown().
 * 2013-09-10: Extracted from test_lib_proto.c.
 * 2013-09-11: Hide the user options data structure.
 * 2014-02-09: Extra verbose output (suppress when not verbose).
 * 2014-04-12: Command line option to list test cases.
 *             API function to initialise test library.
 * 2014-04-21: Ensure options structure is reset before each use.
 * 2014-06-03: Implement cutl_set_cutl_test_setup() to allow the user to set
 *             a function to be called (e.g. to reset stubs) prior to running
 *             each test case.
 * 2014-08-17: Implement cutl_set_cutl_test_teardown().
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "test_lib.h"


static void cutl_setup_default(void);
static void cutl_teardown_default(void);
static void cutl_test_setup_default(void);
static void cutl_test_teardown_default(void);

static char *test_lib_name;
static cutl_test_case_t *test_cases;
static int32_t ntest_cases;
static cutl_user_options_t user_options;

static void (*cutl_setup)(void) = cutl_setup_default;
static void (*cutl_teardown)(void) = cutl_teardown_default;

static void (*cutl_test_setup)(void) = cutl_test_setup_default;
static void (*cutl_test_teardown)(void) = cutl_test_teardown_default;

int32_t cutl_set_test_array(cutl_test_case_t *tests, int32_t ntests)
{
    test_cases = tests;
    ntest_cases = ntests;
    return 0;
}

int32_t cutl_set_test_lib(char *name, cutl_test_case_t *tests, int32_t ntests)
{
    if (NULL != name)
    {
        test_lib_name = name;
    }
    else
    {
        test_lib_name = "Undefined";
    }

    return cutl_set_test_array(tests, ntests);
}

void cutl_usage(void)
{
    printf("cutl C Unit Test Library.\n");
    printf("Copyright (C) 2011-2016 by Iain Nicholson.\n");
    printf("\n");
    printf("Options:\n");
    printf("  -h | --help         This help.\n");
    printf("  -v | --verbose      Verbose output.\n");
    printf("  -l | --list         List test casses.\n");
    printf("  -c | --case <name>  Run specific test case.\n");
    printf("\n");
}

int32_t cutl_test_case_name_to_index(char *name)
{
    int32_t result = -1;
    int32_t i;

    for (i = 0; i < ntest_cases; i++)
    {
        if (! strcmp(name, test_cases[i].name))
        {
            result = i;
            break;
        }
    }

    return result;
}

void cutl_reset_options(void)
{
    user_options.verbose_output      = 0;
    user_options.list_test_cases     = 0;
    user_options.do_single_test_case = 0;
    user_options.test_case           = 0;
}

int32_t cutl_get_options(int argc, char *argv[])
{
    int32_t i = 1;

    cutl_reset_options();

    while (i < argc)
    {
#if 0
        printf("argv[%d]: %s\n", i, argv[i]);
#endif
        if(!strcmp(argv[i], "-h"))
        {
            cutl_usage();
            exit(1);
        }
        else if(!strcmp(argv[i], "--help"))
        {
            cutl_usage();
            exit(1);
        }
        else if(!strcmp(argv[i], "-v"))
        {
            user_options.verbose_output = 1;
        }
        else if(!strcmp(argv[i], "--verbose"))
        {
            user_options.verbose_output=1;
        }
        else if(!strcmp(argv[i], "-l"))
        {
            user_options.list_test_cases = 1;
        }
        else if(!strcmp(argv[i], "--list"))
        {
            user_options.list_test_cases = 1;
        }
        else if ((! strcmp(argv[i], "-c")) || (! strcmp(argv[i], "--case")))
        {
            if ((i+1) < argc)
            {
#if 0
                printf("Single test case: %s.\n", argv[i+1]);
#endif
                if (-1 != (user_options.test_case = cutl_test_case_name_to_index(argv[i+1])))
                {
                    user_options.do_single_test_case = 1;
                }
                else
                {
                    printf("Test case not found: %s.\n", argv[i+1]);
                    exit(1);
                }
                i++;
            }
            else
            {
                printf("Test case expected.\n");
            }
        }
        else
        {
            printf("Unrecognised command line option: %s.\n", argv[i]);
            printf("Try %s -h (or --help) for help.\n", argv[0]);
            exit(1);
        }

        i++;
    }

    return 0;
}

int32_t cutl_run_tests(void)
{
    int32_t tests_executed = 0;
    int32_t i;

    if (1 == user_options.list_test_cases)
    {
        for (i = 0; i < ntest_cases; i++)
        {
            printf("%s\n", test_cases[i].name);
        }
    }
    else
    {
        cutl_setup();

        if (user_options.verbose_output != 0)
        {
            printf("Test library: %s\n", test_lib_name);
        }

        if (1 == user_options.do_single_test_case)
        {
            cutl_test_setup();
            if (user_options.verbose_output != 0)
            {
                printf("Test case: %s ", test_cases[user_options.test_case].name);
            }

            test_cases[user_options.test_case].test();

            if(user_options.verbose_output != 0)
            {
                printf("\n");
            }
            else
            {
                printf(".");
            }

            cutl_test_teardown();

            tests_executed++;
        }
        else
        {
            for (i = 0; i < ntest_cases; i++)
            {
                cutl_test_setup();
                if (user_options.verbose_output != 0)
                {
                    printf("Test case: %s ", test_cases[i].name);
                }

                test_cases[i].test();

                if(user_options.verbose_output != 0)
                {
                    printf("\n");
                }
                else
                {
                    printf(".");
                }

                cutl_test_teardown();

                tests_executed++;
            }
        }

        cutl_teardown();

        if (0 == user_options.verbose_output)
        {
            printf("\n");
        }

        if(user_options.verbose_output != 0)
        {
            printf("Tests executed: %d\n", tests_executed);
        }
    }

    return tests_executed;
}

void cutl_setup_default(void)
{
    if(user_options.verbose_output != 0)
    {
        printf("cutl_setup_default()\n");
    }
}

void cutl_teardown_default(void)
{
    if(user_options.verbose_output != 0)
    {
        printf("cutl_teardown_default()\n");
    }
}

void cutl_test_setup_default(void)
{
    if(user_options.verbose_output != 0)
    {
        printf("cutl_test_setup_default()\n");
    }
}

void cutl_test_teardown_default(void)
{
    if(user_options.verbose_output != 0)
    {
        printf("cutl_test_teardown_default()\n");
    }
}

void cutl_set_cutl_test_setup(void (*test_setup)(void))
{
    if (NULL == test_setup)
    {
        cutl_test_setup = cutl_test_setup_default;
    }
    else
    {
        cutl_test_setup = test_setup;
    }
}

void cutl_set_cutl_test_teardown(void (*test_teardown)(void))
{
    if (NULL == test_teardown)
    {
        cutl_test_teardown = cutl_test_teardown_default;
    }
    else
    {
        cutl_test_teardown = test_teardown;
    }
}

/******************************************************************************/
/* Accessors for unit testing.                                                */
/******************************************************************************/

cutl_user_options_t *cutl_get_user_options(void)
{
    return &user_options;
}

int32_t cutl_get_user_options_verbose_output(void)
{
    return user_options.verbose_output;
}

int32_t cutl_get_user_options_list_test_cases(void)
{
    return user_options.list_test_cases;
}

int32_t cutl_get_user_options_do_single_test_case(void)
{
    return user_options.do_single_test_case;
}

int32_t cutl_get_user_options_test_case(void)
{
    return user_options.test_case;
}


