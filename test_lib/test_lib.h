/*****************************************************************************
 * Unit test library (header).
 * Sat May 28 12:42:41 BST 2011
 * Copyright (C) 2011-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2011-05-29: setup() and teardown().
 * 2013-09-10: Extracted from test_lib_proto.c.
 * 2013-09-11: Hide the user options data structure.
 * 2013-12-11: C++ externs.
 * 2014-04-12: Command line option to list test cases.
 *             API function to initialise test library.
 * 2014-04-21: Accessors for unit testing command line options.
 ******************************************************************************/

#ifndef __TEST_LIB_H__
#define __TEST_LIB_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define NO_OF_ELEMENTS(array)   (int32_t)(sizeof(array)/sizeof(array[0]))

typedef struct
{
    char *name;
    void (*test)(void);
}
cutl_test_case_t;

typedef struct
{
    int32_t verbose_output;
    int32_t list_test_cases;
    int32_t do_single_test_case;
    int32_t test_case;
}
cutl_user_options_t;

int32_t cutl_set_test_array(cutl_test_case_t *tests, int32_t ntests);
int32_t cutl_set_test_lib(char *name, cutl_test_case_t *tests, int32_t ntests);

void cutl_usage(void);
int32_t cutl_get_options(int argc, char *argv[]);
int32_t cutl_run_tests(void);

int32_t cutl_test_case_name_to_index(char *name);

void cutl_set_cutl_setup(void (*setup)(void));
void cutl_set_cutl_teardown(void (*teardown)(void));

void cutl_set_cutl_test_setup(void (*test_setup)(void));
void cutl_set_cutl_test_teardown(void (*test_teardown)(void));

/*
 * For unit testing:
 */
cutl_user_options_t *cutl_get_user_options(void);
int32_t              cutl_get_user_options_verbose_output(void);
int32_t              cutl_get_user_options_list_test_cases(void);
int32_t              cutl_get_user_options_do_single_test_case(void);
int32_t              cutl_get_user_options_test_case(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __TEST_LIB_H__ */

