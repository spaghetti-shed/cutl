/*
 * pthread_stubs.h - Basic unit test stubs and accessors for pthread functions
 *                   (header).
 *
 * Tue Feb 25 16:48:16 GMT 2025
 *
 * Copyright (C) 2025 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2025-02-25 Initial creation.
 */

#include <stdint.h>
#include <stdio.h>

#ifndef __PTHREAD_STUBS_H__
#define __PTHREAD_STUBS_H__

#if 0
#ifdef __cplusplus
extern "C"
{
#endif
#endif

#define pthread_mutex_trylock     stub_pthread_mutex_trylock
#define pthread_mutex_unlock      stub_pthread_mutex_unlock
#define pthread_mutexattr_init    stub_pthread_mutexattr_init
#define pthread_mutexattr_destroy stub_pthread_mutexattr_destroy
#define pthread_mutex_init        stub_pthread_mutex_init
#define pthread_mutex_destroy     stub_pthread_mutex_destroy

int stub_pthread_mutex_trylock(pthread_mutex_t *mutex);
int stub_pthread_mutex_unlock(pthread_mutex_t *mutex);
int stub_pthread_mutexattr_init(pthread_mutexattr_t *attr);
int stub_pthread_mutexattr_destroy(pthread_mutexattr_t *attr);
int stub_pthread_mutex_init(pthread_mutex_t *restrict mutex,
                            const pthread_mutexattr_t *restrict attr);
int stub_pthread_mutex_destroy(pthread_mutex_t *mutex);

#if 0
#ifdef __cplusplus
}
#endif
#endif

#endif /* __PTHREAD_STUBS_H__ */

