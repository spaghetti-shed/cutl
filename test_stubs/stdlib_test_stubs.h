/*****************************************************************************
 * stdlib_test_stubs (header) - Basic unit test stubs and accessors for
 * stdlib functions (header).
 * Tue Jun  3 20:58:38 BST 2014
 * Copyright (C) 2014-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2014-06-03 Initial creation.
 *
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __STDLIB_TEST_STUBS_H__
#define __STDLIB_TEST_STUBS_H__

#ifdef __cplusplus
extern "C"
{
#endif

void *cutl_counting_malloc(size_t size);
void cutl_counting_free(void *ptr);
void cutl_counting_exit(int status);

void cutl_stdlib_test_stubs_reset(void);

int32_t cutl_counting_malloc_get_count(void);
int32_t cutl_counting_free_get_count(void);
int32_t cutl_counting_exit_get_count(void);

size_t cutl_counting_malloc_get_size(void);
void  *cutl_counting_free_get_ptr(void);
int    cutl_counting_exit_get_status(void);

void cutl_counting_malloc_set_null_flag(int32_t flag);

#ifdef __cplusplus
}
#endif

#endif /* __STDLIB_TEST_STUBS_H__ */

