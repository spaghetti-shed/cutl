/*
 * unistd_stubs.h - Basic unit test stubs and accessors for unistd functions
 *                  (header).
 *
 * Tue Feb 25 21:20:18 GMT 2025
 *
 * Copyright (C) 2025 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2025-02-25 Initial creation.
 */

#include <stdint.h>
#include <stdio.h>

#ifndef __UNISTD_STUBS_H__
#define __UNISTD_STUBS_H__

#if 0
#ifdef __cplusplus
extern "C"
{
#endif
#endif

#define usleep                    stub_usleep

int stub_usleep(useconds_t usec);

#if 0
#ifdef __cplusplus
}
#endif
#endif

#endif /* __UNISTD_STUBS_H__ */

