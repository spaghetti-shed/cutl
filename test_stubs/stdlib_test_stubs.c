/*****************************************************************************
 * stdlib_test_stubs - Basic unit test stubs and accessors for stdlib
 * functions.
 * Tue Jun  3 20:58:38 BST 2014
 * Copyright (C) 2014-2016 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2014-06-03 Initial creation.
 *
 ******************************************************************************/

#include <stdlib.h>
#include "stdlib_test_stubs.h"

static int32_t counting_malloc_count;
static int32_t counting_free_count;
static int32_t counting_exit_count;

static void *counting_malloc_ptr;
size_t       counting_malloc_size;
int32_t      counting_malloc_null_flag;
static void *counting_free_ptr;
static int   counting_exit_status;

void cutl_stdlib_test_stubs_reset(void)
{
    counting_malloc_count = 0;
    counting_free_count   = 0;
    counting_exit_count   = 0;

    counting_malloc_ptr       = NULL;
    counting_malloc_size      = 0;
    counting_malloc_null_flag = 0;
    counting_free_ptr         = NULL;
    counting_exit_status      = EXIT_SUCCESS;
}

void *cutl_counting_malloc(size_t size)
{
    void *retval = NULL;

    counting_malloc_count++;
    counting_malloc_size = size;

    if (counting_malloc_null_flag)
    {
    }
    else
    {
        counting_malloc_ptr = malloc(size);
        retval = counting_malloc_ptr;
    }

    return retval;
}

void cutl_counting_free(void *ptr)
{
    counting_free_count++;
    free(ptr);
    counting_free_ptr = ptr;
}

void cutl_counting_exit(int status)
{
    counting_exit_count++;
    counting_exit_status = status;
}

int32_t cutl_counting_malloc_get_count(void)
{
    return counting_malloc_count;
}

int32_t cutl_counting_free_get_count(void)
{
    return counting_free_count;
}

int32_t cutl_counting_exit_get_count(void)
{
    return counting_exit_count;
}

size_t cutl_counting_malloc_get_size(void)
{
    return counting_malloc_size;
}

void *cutl_counting_free_get_ptr(void)
{
    return counting_free_ptr;
}

int cutl_counting_exit_get_status(void)
{
    return counting_exit_status;
}

void cutl_counting_malloc_set_null_flag(int32_t flag)
{
    counting_malloc_null_flag = flag;
}

