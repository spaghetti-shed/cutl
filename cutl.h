/******************************************************************************
 * cutl (header) - C Unit Test Library (header).
 * Wed Dec 11 22:06:07 GMT 2013
 * Copyright (C) 2010-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of cutl.
 *
 * cutl is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * cutl is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cutl; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2013-12-11 High-level include file for cutl assertions, test functions and
 *            unit test library framework.
 * 2014-06-03 Include stdlib stubs header (very basic stubs).
 ******************************************************************************/

#ifndef __CUTL_H__
#define __CUTL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "assertions/assertions.h"
#include "test_functions/test_functions.h"
#include "test_lib/test_lib.h"
#include "test_stubs/stdlib_test_stubs.h"

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __CUTL_H__ */

