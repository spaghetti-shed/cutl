#
# Makefile for C Unit Test Library (cutl).
# Copyright (C) 2006-2025 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of cutl.
#
# cutl is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# cutl is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with cutl; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# Tue Jul  9 22:19:02 BST 2013
#
# Modification History:
# 2013-07-09 Initial creation.
# 2013-09-10 Added test_lib target.
# 2014-01-03 Public static library in top dir.
# 2014-06-03 Added test_stubs subdir for basic C library function stubs.
# 2015-01-06 Added double-precision floating-point tests.
# 2015-01-12 Added q31 fixed-point tests.
# 2022-06-03 Fixed compilation of test_stubs subdir.
# 2022-06-11 Upate install target for cutlmacro utility.
# 2025-02-25 Add pthread stubs.
#            Add unistd stubs.
#

CC=gcc
CFLAGS=-Wall -pedantic -O -fPIC
SHAREDFLAGS=-shared
INSTALL_DIR=/usr/local
INSTALL_INC=$(INSTALL_DIR)/include
INSTALL_LIB=$(INSTALL_DIR)/lib64
INSTALL_BIN=$(INSTALL_DIR)/bin

.PHONY: assertions
.PHONY: test_functions
.PHONY: test_lib
.PHONY: test_stubs

all: assertions test_stubs test_functions test_lib static shared

test_functions: assertions
	$(MAKE) -C  test_functions

assertions:
	$(MAKE) -C assertions

test_stubs:
	$(MAKE) -C  test_stubs

test_lib:
	$(MAKE) -C  test_lib

static: test_functions assertions test_lib
	ar -rcs cutl.a assertions/assertions.o test_functions/test_functions.o \
		test_functions/raw_list.o test_lib/test_lib.o \
		test_stubs/stdlib_test_stubs.o \
		test_stubs/pthread_stubs.o \
		test_stubs/unistd_stubs.o

shared: test_functions assertions test_lib
	$(CC) $(CFLAGS) $(SHAREDFLAGS) -o libcutl.so \
		assertions/assertions.o test_functions/test_functions.o \
		test_functions/raw_list.o test_lib/test_lib.o \
		test_stubs/stdlib_test_stubs.o \
		test_stubs/pthread_stubs.o \
		test_stubs/unistd_stubs.o

install:
	mkdir -p $(INSTALL_INC)/assertions
	cp assertions/assertions.h $(INSTALL_INC)/assertions
	mkdir -p $(INSTALL_INC)/test_functions
	cp test_functions/test_functions.h $(INSTALL_INC)/test_functions
	mkdir -p $(INSTALL_INC)/test_lib
	cp test_lib/test_lib.h $(INSTALL_INC)/test_lib
	mkdir -p $(INSTALL_INC)/test_stubs
	cp test_stubs/stdlib_test_stubs.h $(INSTALL_INC)/test_stubs
	cp test_stubs/pthread_stubs.h $(INSTALL_INC)/test_stubs
	cp test_stubs/unistd_stubs.h $(INSTALL_INC)/test_stubs
	cp cutl.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp libcutl.so $(INSTALL_LIB)
	mkdir -p $(INSTALL_BIN)
	cp assertions/cutlmacro $(INSTALL_BIN)
	chmod +x assertions/cutlmacro

uninstall:
	rm -f $(INSTALL_INC)/assertions/assertions.h
	rm -f $(INSTALL_INC)/test_functions/test_functions.h
	rm -f $(INSTALL_INC)/test_lib/test_lib.h
	rm -f $(INSTALL_INC)/test_stubs/stdlib_test_stubs.h
	rm -f $(INSTALL_INC)/test_stubs/pthread_stubs.h
	rm -f $(INSTALL_INC)/test_stubs/unistd_stubs.h
	rm -f $(INSTALL_INC)/cutl.h
	rm -f $(INSTALL_LIB)/libcutl.so
	rm -f $(INSTALL_BIN)/cutlmacro

clean:
	$(MAKE) -C assertions clean
	$(MAKE) -C test_functions clean
	$(MAKE) -C test_lib clean
	$(MAKE) -C test_stubs clean
	rm -f cutl.a
	rm -f libcutl.so

